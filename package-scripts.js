const
  _ = require('lodash'),
  { crossEnv, concurrent, series } = require('nps-utils');

function seq(...args) {
  return args.join(';') + ';true';
}

seq.nps = function(...args) {
  return seq.apply(null, _.map(args, (a) => `nps -s "${a}"`));
}

module.exports = {
  scripts: {
    test: {
      default: 'mocha test',
      coverage: 'istanbul cover ./node_modules/.bin/_mocha -- test',
      watch: 'multi="mocha-notifier-reporter=- spec=-" mocha test -R mocha-multi'
    },
    lint: {
      default: seq.nps('lint.eslint', 'lint.jshint'),
      builder: seq.nps('lint.eslint --format unix', 'lint.jshint --reporter unix'),
      
      eslint: 'eslint --config ./.eslintrc.yml pipo test',
      jshint: 'jshint --config ./.jshintrc pipo test',

      badge: './scripts/mkbadge'
    },
    docker: {
      test: [
        'docker run --rm -it',
          '-v "$PWD":/usr/src/myapp', '-w /usr/src/myapp', 'node:alpine',
          'sh -c \'npm install && npm run test\'' ].join(' '),
      build: 'docker build -f ./data/Dockerfile -t pipo:latest .'
    },
    jsdoc: 'rm -rf docs && jsdoc -c jsdoc.json',
    install: {
      applet: 'node ./applet.js --install'
    }
  }
};
