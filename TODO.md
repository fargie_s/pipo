

*   [ ] HTTPSFetcher to tests SSL support on given proxy
*   [x] Implement a nicer usage
*   [x] Write a README
*   [x] Add progress messages ?
*   [x] Support CSON pipes
*   [ ] Unittests on registry loader
*   [X] Cmdline applets
*   [X] console help support

Pipes to be written :
*   [ ] UDPIn/Out
*   [ ] MCastOut
*   [x] SqliteOut
*   [ ] FileOut
*   [ ] RSSFeedOut
*   [ ] SyslogOut
*   [ ] MailErrorOut
*   [ ] RSSFeedIn
*   [x] SqliteQuery --> SQLiteIn/Out/Eval
*   [ ] ShellEval
*   [ ] FilterFile & FindFile
*   [x] MailIn/MailOut
*   [ ] TCPIn/Out
*   [ ] SubPipe fork

Rename Replace as Substr ?

Reconfig WaitFor --> rebloquer
