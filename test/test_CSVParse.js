'use strict';

const
  should = require('should'),
  describe = require('mocha').describe,
  it = require('mocha').it;

const
  pipo = require('../pipo');


describe('CSVParse', function() {

  it('parses rows', function(done) {
    var pipe = new pipo.CSVParse();
    var ret = [];

    pipe.on('item', function(item) { ret.push(item); });
    pipe.onItem({ csv: "12,13,14\n15,16,blue" });
    pipe.once('end', function() {
      should(ret).size(2);
      should(ret[0]).have.property('row').eql([ 12, 13, 14 ]);
      should(ret[1]).have.property('row').eql([ 15, 16, "blue" ]);
      done();
    });
  });

  it('can parse columns', function(done) {
    var pipe = new pipo.CSVParse();
    var ret = [];

    pipe.on('item', function(item) { ret.push(item); });
    pipe.onItem({
      CSVParseConfig: { columns: true, delimiter: ';' },
      csv: "first;last;age\nBob;Sponge;14"
    });
    pipe.once('end', function() {
      should(ret).size(1);
      should(ret[0]).eql({ first: 'Bob', last: 'Sponge', age: 14 });
      done();
    });
  });

  it('can use configured columns', function(done) {
    var pipe = new pipo.CSVParse();
    var ret = [];

    pipe.on('item', function(item) { ret.push(item); });
    pipe.onItem({
      CSVParseConfig: { columns: [ 'first', 'last', 'age' ] },
      csv: "Bob,Sponge,14"
    });
    pipe.once('end', function() {
      should(ret).size(1);
      should(ret[0]).eql({ first: 'Bob', last: 'Sponge', age: 14 });
      done();
    });
  });

  it('fails on partial contents', function(done) {
        var pipe = new pipo.CSVParse();

        pipe.on('item', function(item) {
          should(item).have.property('errorString').containEql('Length');
          done();
        });
        pipe.onItem({
          CSVParseConfig: { columns: [ 'first', 'last', 'age' ] },
          csv: "Bob,Sponge"
        });
  });
});
