'use strict';

const
  should = require('./should-ext'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');

describe('Elapsed', function() {

  it('measures elapsed time', function(done) {
    var pipe = new pipo.Elapsed();

    pipe.once('item', function(item) {
      should(item).have.property('elapsed').lessThan(100);

      /* y is unrelated but should be kept */
      should(item).have.property('y').eql(12);
      done();
    });
    pipe.onItem({ y: 12 });
  });

  it('can use a custom property', function(done) {
    var pipe = new pipo.Elapsed();

    pipe.once('item', function(item) {
      should(item).have.property('time').lessThan(100);
      done();
    });
    pipe.onItem({
      ElapsedConfig: { property: 'time' },
      y: 12
    });
  });

  it('can be disabled', function(done) {
    var pipe = new pipo.Elapsed();

    pipe.once('item', function(item) {
      should(item).eql({ y: 12 });
      done();
    });
    pipe.onItem({
      ElapsedConfig: { property: null },
      y: 12
    });
  });
});
