'use strict';

const
  _ = require('lodash'),
  should = require('should'),
  tmp = require('tmp'),
  path = require('path'),
  debug = require('debug')('pipo:tests'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  pipo = require('../pipo');


describe('SQLiteOut', function() {
  var dir;

  beforeEach(function(done) {
    dir = tmp.dirSync({ unsafeCleanup: true });
    debug('tmpdir:', dir.name);

    /* prepare a test database */
    var pipe = new pipo.SQLiteEval();
    pipe.once('end', () => done());

    pipe.onItem({
      SQLiteEvalConfig: { database: path.join(dir.name, 'test.db') },
      sql: 'CREATE TABLE test (date INTEGER PRIMARY KEY, name TEXT NOT NULL)'
    });
  });

  afterEach(function() {
    dir.removeCallback();
    dir = null;
  });

  function checkDB(content, done) {
    var pipe = new pipo.SQLiteIn();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.on('end', () => {
      should(items).eql(content);
      done();
    });

    pipe.onItem({
      SQLiteInConfig: {
        database: path.join(dir.name, 'test.db'), table: 'test'
      },
      where: true
    });
  }

  function testSql(input, messages, db, done) {
    var pipe = new pipo.SQLiteOut();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.once('end', () => {
      should(items).eql(messages);
      checkDB(_.isNil(db) ? messages : db, done);
    });
    pipe.onItem(input);
  }

  it('can send an item in a database', function(done) {
    testSql({
      SQLiteOutConfig: {
        database: path.join(dir.name, 'test.db'),
        table: 'test'
      },
      date: 1234, name: 'toto'
    }, [ { date: 1234, name: 'toto' } ], null, done);
  });

  it('fails on missing column', function(done) {
    testSql({
      SQLiteOutConfig: {
        database: path.join(dir.name, 'test.db'),
        table: 'test'
      },
      date: 1234 /* no 'name' on purpose */
    }, [
      { errorString: "NOT NULL constraint failed: test.name" },
      { date: 1234 }
    ], [], done);
  });

  it('fails to access an invalid database', function(done) {
    testSql(
      {
        SQLiteOutConfig: { database: '', table: 'test' },
        date: 1234, name: 'toto'
      },
      [
        { errorString: 'A database filename cannot be an empty string' },
        { date: 1234, name: 'toto' }
      ], [], done);
  });

  it('fails on missing table information', function(done) {
    testSql(
      {
        SQLiteOutConfig: { database: path.join(dir.name, 'test.db') },
        date: 1234, name: 'toto'
      },
      [
        { errorString: 'SQLite table not set' },
        { date: 1234, name: 'toto' }
      ],
      [], done);
  });
});
