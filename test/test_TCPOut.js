'use strict';

const
  _ = require('lodash'),
  should = require('./should-ext'),
  { describe, it } = require('mocha'),
  net = require('net');

const
  pipo = require('../pipo');

describe('TCPOut', function() {
  it('ends once connection is terminated', function(done) {
    var tcpIn = new pipo.TCPIn();
    var tcpOut = new pipo.TCPOut();
    var items = [];

    tcpIn.onItem({ TCPInConfig: { address: 'tcp://127.0.0.1:12334', autoClose: false } });
    tcpOut.onItem({ TCPOutConfig: { address: 'tcp://127.0.0.1:12334' } });
    tcpOut.onItem({ name: "test" });

    tcpIn.on('item', (item) => items.push(item));
    tcpOut.once('end', () => {
      should(items).eql([ { name: "test"} ]);
      should(tcpOut._conn).be.null();
      tcpIn.onItem({ TCPInConfig: { address: null } });
    });
    tcpIn.once('end', () => {
      done();
    });
  });

  it('sends an error when upstream hangs-up', function(done) {
    var tcpIn = new pipo.TCPIn();
    var tcpOut = new pipo.TCPOut();
    var items = [];

    tcpOut.ref();
    tcpOut.on('item', function(item) {
      items.push(item);
      tcpOut.unref();
    });
    tcpOut.on('end', function() {
      should(items).have.size(1);
      should(items[0]).have.property('errorString');
      done();
    });

    tcpIn.onItem({ TCPInConfig: { address: 'tcp://127.0.0.1:12334' } });
    tcpOut.onItem({ TCPOutConfig: { address: 'tcp://127.0.0.1:12334' } });
    /* wait a bit for the connection */
    _.delay(function() { tcpIn.onItem({ TCPInConfig: { address: null } }); }, 200);
  });
});
