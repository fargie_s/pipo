
const
  { describe, it } = require('mocha'),
  q = require('../pipo/utils/q-ext'),
  _ = require('lodash'),
  should = require('should');

class MFBind0Test {
  foo(bound, callback) {
    should(bound).eql('bound');
    callback(null, 'result');
  }
}

describe('q-ext', function() {
  it('exports q', function() {
    should(q).equal(require('q')); // eslint-disable-line global-require
  });

  describe('reduce', function() {
    it('can reduce static array', function() {
      return q.reduce([1, 2, 3], function(res, item, key, values) {
        should(item).eql(key + 1);
        should(values).property(key).eql(item);
        return item + (_.isNil(res) ? 0 : res);
      })
      .then(function(ret) { should(ret).eql(6); });
    });

    it('can reduce promise', function() {
      return q([1, 2, 3]).reduce(function(res, item, key, values) {
        should(item).eql(key + 1);
        should(values).property(key).eql(item);
        return item + (_.isNil(res) ? 0 : res);
      })
      .then(function(ret) { should(ret).eql(6); });
    });
  });

  describe('mbind', function() {
    it('can mbind a class', function() {
      var c = new MFBind0Test();
      return q()
      .then(q.nmbind(c, 'foo', 'bound'))
      .then(function(value) { should(value).eql('result'); });
    });
  });

  describe('Lock', function() {
    it('can lock a promise', function() {
      var lock = new q.Lock(2);
      should(lock).property('count').eql(2);

      return q(12)
      .then(lock.wait.bind(lock))
      .then(function(value) {
        should(lock).property('count').eql(1);
        should(value).eql(12);
      })
      .then(lock.wait.bind(lock))
      .then(function() {
        var counter = 0;
        var prom = lock.wait()
        .then(function() { counter += 1; });

        should(counter).eql(0);
        lock.post();
        return prom
        .then(function() {
          should(lock).property('count').eql(0);
          should(counter).eql(1);
        });
      })
      .then(lock.post.bind(lock))
      .then(lock.post.bind(lock))
      .then(function() {
        should(lock).property('count').eql(2);
      });
    });
  });

  describe('Throttle', function() {
    it('can Throttle a function', function() {
      var throttle = new q.Throttle(100); /* 100 ms wait time */

      var count = 0;
      var prom = throttle.promise()
      .then(() => { count += 1; })
      .then(throttle.promise.bind(throttle))
      .then(() => { count += 1; });

      should(count).eql(0);
      return q().delay(150)
      .then(function() {
        should(count).eql(1);
        return prom;
      })
      .then(() => { should(count).eql(2); });
    });

    it('runs directly when possible', function() {
      var throttle = new q.Throttle(100);

      return q.delay(100)
      .then(function() {
        should(throttle.promise().inspect())
        .property('state').eql('fulfilled');
      });
    });
  });

  describe('retry', function() {
    it('retries max 3 times', function() {
      var ret = 0;
      return q.retry(
        () => { ret += 1; throw 'error'; }, () => { return true; })
      .then(
        () => { throw 'should fail'; },
        (err) => {
          should(err).eql('error');
          should(ret).eql(4);
        });
    });

    it('retries as much as I want', function() {
      var ret = 0;
      return q.retry(
        () => { ret += 1; throw 'error'; }, () => { return true; }, 10)
      .then(
        () => { throw 'should fail'; },
        (err) => {
          should(err).eql('error');
          should(ret).eql(11);
        }
      );
    });

    it('does not retry on success', function() {
      return q.retry(
        () => { return 1; }, () => { return true; })
      .then((ret) => { should(ret).eql(1); });
    });

    it('stops trying when I want', function() {
      var ret = 0;
      return q.retry(
        () => { ret += 1; throw ret; }, (err) => { return err !== 4; }, 10)
      .then(
        () => { throw 'should fail'; },
        (err) => {
          should(err).eql(4);
          should(ret).eql(4);
        }
      );
    });

    it('handles a failing check', function() {
      return q.retry(() => { throw 42; }, () => { throw 24; })
      .then(
        () => { throw 'should fail'; },
        (err) => {
          should(err).eql(24);
        }
      );
    });

    it('retries a promise', function() {
      var ret = 0;
      return q(40)
      .retry(
        (value) => {
          ret += 1;
          if (value === 40) { throw 'error'; }
        },
        () => { return true; }
      )
      .then(
        () => { throw 'should fail'; },
        (err) => {
          should(ret).eql(4);
          should(err).eql('error');
        }
      );
    });
  });
});
