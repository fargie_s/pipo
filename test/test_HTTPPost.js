'use strict';

const
  should = require('should'),
  { describe, it, before, after } = require('mocha');

const
  http = require('http'),
  pipo = require('../pipo');


describe('HTTPPost', function() {
  var stubServer;
  var addr;

  before(function() {
    stubServer = http.createServer(function(req, res) {
      req.body = '';
      req.on('data', function(data) { req.body += data; });
      req.once('end', () => {
        req.removeAllListeners('data');
        should(req.headers).have.property('content-type',
          'application/x-www-form-urlencoded');
        should(req.body).containEql('format=2');
        should(req.body).containEql('op=Go');
        res.writeHead(200, {'Content-Type': 'application/json'});
        res.end(JSON.stringify('ok'));
      });
    });
    stubServer.listen();
    addr = stubServer.address();
  });
  after(function() {
    stubServer.close();
    stubServer = null;
  });

  it('fetches messages', function(done) {
    var pipe = new pipo.HTTPPost();

    pipe.on('item', (item) => {
      should(item).have.property('body', 'ok');
      should(item).have.property('headers')
      .containEql({ 'content-type': 'application/json' });
      done();
    });
    pipe.onItem({
      url: `http://${addr.address}:${addr.port}`,
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      data: { format: 2, op: "Go" }
    });
  });

  it('fails with error', function(done) {
    var pipe = new pipo.HTTPPost();

    pipe.on('item', (item) => {
      should(item).have.property('errorString')
      .containEql('ECONNREFUSED');
      done();
    });
    pipe.onItem({ url: `http://${addr.address}:${addr.port + 1}` });
  });
});
