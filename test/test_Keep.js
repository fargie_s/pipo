'use strict';

const
  should = require('should'),
  describe = require('mocha').describe,
  it = require('mocha').it;

const
  pipo = require('../pipo');


describe('Keep', function() {

  it('keeps requested attributes', function(done) {
    var pipe = new pipo.Keep();

    pipe.on('item', (item) => {
      should(item).eql({ test: 42 });
      done();
    });
    pipe.onItem({ KeepConfig : { properties: [ 'test', 'titi' ] } });
    pipe.onItem({ test: 42, toto: 44 });
    pipe.end(0);
  });
});
