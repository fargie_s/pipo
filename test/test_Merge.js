'use strict';

const
  should = require('./should-ext'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');

describe('Merge', function() {

  it('merges items', function(done) {
    var pipe = new pipo.Merge();

    pipe.on('item', function(item) {
      should(item).eql({ toto: { value1: 1, value2: 2, value3: 3 } });
      done();
    });
    pipe.onItem({ toto: { value1: 1, value2: 1, value3: 1 }});
    pipe.onItem({ toto: { value2: 2, value3: 2 }});
    pipe.onItem({ toto: { value3: 3 }});
  });
});
