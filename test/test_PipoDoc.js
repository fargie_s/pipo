'use strict';

const
  _ = require('lodash'),
  should = require('./should-ext'),
  describe = require('mocha').describe,
  stream = require('stream'),
  it = require('mocha').it;

const
  pipo = require('../pipo'),
  PipoDoc = require('../pipo/utils/PipoDoc');

describe('PipoDoc', function() {

  it('fails to document inexisting item', function() {
    var pass = new stream.PassThrough();
    var doc = new PipoDoc(pass);

    should(doc.jsDoc("NotExist")).eql(false);
    should(pass.read().toString()).containEql('Failed to');
  });

  _.forEach(_.keys(pipo.Registry.pipes), function(elt) {
    it(`documents ${elt}`, function() {
      var pass = new stream.PassThrough();
      var doc = new PipoDoc(pass);

      should(doc.jsDoc(elt)).eql(true);
      var text = pass.read().toString();
      should(text).containEql('DESCRIPTION');
    });
  });
});
