'use strict';

const
  should = require('should'),
  describe = require('mocha').describe,
  it = require('mocha').it;

const
  pipo = require('../pipo');


describe('ChildPipe', function() {

  it('create a subpipe', function(done) {
    var pipe = new pipo.ChildPipe();
    var items = [];

    pipe.onItem({ "pipe": "Aggregate" });
    pipe.onItem({ "item": 42 });
    pipe.on('item', (i) => items.push(i));
    pipe.once('end', () => {
      should(items).eql([ { items: [ { item: 42 } ] } ]);
      done();
    });
  });

  it('forwards messages when no pipe set', function(done) {
    var pipe = new pipo.ChildPipe();
    pipe.on('item', function() {
      done();
    });
    pipe.onItem({ 'item': 1 });
  });

    it('handles oneShot pipes', function(done) {
      var pipe = new pipo.ChildPipe();

      pipe.on('item', function(item) {
        should(item).have.property('items')
        .eql([ { item: 42 } ]);
        done();
      });
      pipe.onItem({
        'ChildPipeConfig': {
          'pipe': 'Aggregate',
          'oneShot': true
        },
        'item': 42
      });
    });

    it('terminates oneShot pipes', function(done) {
      var pipe = new pipo.ChildPipe();
      var calls = 0;

      pipe.on('item', () => {
        calls += 1;
      });
      pipe.on('end', function() {
        should(calls).eql(2);
        done();
      });
      pipe.onItem({
        'ChildPipeConfig': {
          'pipe': 'Aggregate',
          'oneShot': true
        }
      });
      pipe.onItem({ 'item': 42 });
      pipe.onItem({ 'item': 42 });
    });
});
