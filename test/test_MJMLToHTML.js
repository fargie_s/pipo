'use strict';

const
  should = require('should'),
  { it, describe } = require('mocha');

const
  pipo = require('../pipo');

describe('MJMLToHTML', function() {

  it('can convert an MJML document', function(done) {
    var pipe = new pipo.MJMLToHTML();

    pipe.once('item', function(item) {
      should(item).have.property('html')
      .containEql('<body>');
      done();
    });
    pipe.onItem({
      mjml: "<mjml><mj-body><mj-raw>test</mj-raw></mj-body></mjml>"
    });
  });

  it('can use another property', function(done) {
    var pipe = new pipo.MJMLToHTML();

    pipe.once('item', function(item) {
      should(item).have.property('test')
      .containEql('<body>');
      done();
    });
    pipe.onItem({
      MJMLToHTMLConfig: { property: 'test' },
      test: "<mjml><mj-body><mj-raw>test</mj-raw></mj-body></mjml>"
    });
  });

  it('emits errors', function(done) {
    var pipe = new pipo.MJMLToHTML();
    var items = [];

    pipe.on('item', (i) => items.push(i));
    pipe.once('end', function() {
      should(items).size(3);

      should(items[0]).have.property('errorString')
      .containEql('Element mj-xxx');

      should(items[1]).have.property('errorString')
      .containEql('Element a');

      should(items[2]).have.property('html')
      .containEql('test1')
      .not.containEql('test2');
      done();
    });
    pipe.onItem({
      mjml: "<mjml><mj-body><mj-raw>test1</mj-raw><mj-xxx>test2</mj-xxx><a></mj-body></mjml>"
    });
  });

  it('can keepComments', function(done) {
    var pipe = new pipo.MJMLToHTML();
    var items = [];

    pipe.on('item', (i) => items.push(i));
    pipe.onItem({
      mjml: "<mjml><mj-body><!-- comment --><mj-raw>test</mj-raw></mj-body></mjml>"
    });

    pipe.onItem({
      MJMLToHTMLConfig: { keepComments: true },
      mjml: "<mjml><mj-body><!-- comment --><mj-raw>test</mj-raw></mj-body></mjml>"
    });

    pipe.once('end', () => {
      should(items).size(2);
      should(items[0]).have.property('html')
      .not.containEql('comment');

      should(items[1]).have.property('html')
      .containEql('comment');
      done();
    });
  });
});
