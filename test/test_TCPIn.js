'use strict';

const
  _ = require('lodash'),
  should = require('./should-ext'),
  { describe, it } = require('mocha'),
  net = require('net');

const
  pipo = require('../pipo');

describe('TCPIn', function() {

  it('can receive messages', function(done) {
    var tcpIn = new pipo.TCPIn();
    var tcpOut = new pipo.TCPOut();
    var items = [];

    tcpIn.on('item', function(item) {
      items.push(item);
      // close connection
      tcpIn.onItem({ TCPInConfig: { address: null } });
    });
    tcpIn.on('end', function() {
      should(items).eql([ { name: 'test' } ]);
      done();
    });
    tcpIn.onItem({ TCPInConfig: { address: 'tcp://127.0.0.1:12334' } });
    tcpOut.onItem({
      TCPOutConfig: { address: 'tcp://127.0.0.1:12334' },
      name: 'test'
    });
  });

  it('sends error on invalid address', function(done) {
    var tcpIn = new pipo.TCPIn();

    tcpIn.on('item', function(item) {
      should(item).eql({ errorString: 'failed to parse address: pwet://127.0.0.1:12334' });
      done();
    });
    tcpIn.onItem({ TCPInConfig: { address: 'pwet://127.0.0.1:12334' } });
  });

  it('multiplexes streams', function(done) {
    var tcpIn = new pipo.TCPIn();
    var items = [];

    tcpIn.on('item', function(item) { items.push(item); });
    tcpIn.on('end', function() {
      should(items).eql([ { name: 'c0' }, { name: 'c1' } ]);
      done();
    });
    tcpIn.onItem({ TCPInConfig: { address: 'tcp://127.0.0.1:12334' } });
    _.delay(function() {
      var c = [
        net.createConnection({ host: '127.0.0.1', port: 12334 }),
        net.createConnection({ host: '127.0.0.1', port: 12334 }) ];

      c[0].write('{ "name": "');
      c[1].write('{ "name": "');
      _.delay(function() {
        c[0].write('c0" }');
        c[0].end();
        c[1].write('c1" }');
        c[1].write('{ "TCPInConfig": { "address": null } }')
        c[1].end();
      }, 200);
    }, 200);
  });

  it('autoClose closes once all clients are done', function(done) {
    var tcpIn = new pipo.TCPIn();
    var tcpOut = [ new pipo.TCPOut(), new pipo.TCPOut() ];
    var items = [];

    tcpIn.on('item', (item) => items.push(item));
    tcpIn.onItem({ TCPInConfig: { address: 'tcp://127.0.0.1:12334', autoClose: true } });
    tcpOut[0].onItem({ TCPOutConfig: { address: 'tcp://127.0.0.1:12334' } });
    tcpOut[1].onItem({ TCPOutConfig: { address: 'tcp://127.0.0.1:12334' } });
    tcpOut[0].onItem({ name: "test" });
    tcpOut[1].onItem({ name: "test" });
    tcpIn.once('end', () => {
      should(items).eql([ { name: "test" }, { name: "test" } ]);
      /* wait a bit for TCPOut to terminate */
      _.delay(function() {
        should(tcpOut[0]).have.property('_ref', 0);
        should(tcpOut[1]).have.property('_ref', 0);
        done();
      }, 100);
    });
  });
});
