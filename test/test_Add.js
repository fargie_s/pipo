'use strict';

const
  should = require('./should-ext'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');

describe('Add', function() {

  it('adds a property', function(done) {
    var pipe = new pipo.Add();

    pipe.on('item', function(item) {
      should(item).not.have.property('toto.titi[0].tata');
      should(item).get('toto.titi[0].tata').eql(42);
      done();
    });
    pipe.onItem({
      "AddConfig": {
        "property": "toto.titi[0].tata",
        "value": 42
      },
      "item": 42
    });
  });

  it('merges an item', function(done) {
    var pipe = new pipo.Add();

    pipe.on('item', function(item) {
      should(item).eql({ "toto": 42,
        "titi": { "tata": 44, "tutu": 42 },
        "tutu": { "tata": 44 }
      });
      done();
    });
    pipe.onItem({
      "AddConfig": {
        "item": {
          "toto": 42,
          "titi.tata": 44, /* expand titi */
          "tutu": { "tata": 44 } /* override tutu */
        }
      },
      "titi": { "tutu": 42 },
      "tutu": { "tutu": 42 }
    });
  });
});
