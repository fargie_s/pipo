'use strict';

const
  fs = require('fs'),
  path = require('path'),
  should = require('should'),
  describe = require('mocha').describe,
  it = require('mocha').it,
  tmp = require('tmp');

const
  pipo = require('../pipo');


describe('RenameFile', function() {

  it('rename a file', function(done) {
    var tmpdir = tmp.dirSync({ unsafeCleanup: true });
    var tmpfile = tmp.fileSync({ prefix: 'test', dir: tmpdir.name });
    var pipe = new pipo.RenameFile();

    pipe.once('item', (item) => {
      should(item).have.property('file', path.join(tmpdir.name, 'test.txt'));
      should.ok(fs.existsSync(item.file));
      tmpfile.removeCallback();
      tmpdir.removeCallback();
      done();
    });

    pipe.onItem({ "file" : tmpfile.name, "dest" : "test.txt" });
  });

  it('moves a file in a directory', function(done) {
    var tmpdir = tmp.dirSync({ unsafeCleanup: true });
    var tmpfile = tmp.fileSync({ prefix: 'test', dir: tmpdir.name });
    var tmpdest = tmp.dirSync({ unsafeCleanup: true });
    var pipe = new pipo.RenameFile();

    pipe.once('item', (item) => {
      should(item).have.property('file',
        path.join(tmpdest.name, path.basename(tmpfile.name)));
      should.ok(fs.existsSync(item.file));
      tmpfile.removeCallback();
      tmpdir.removeCallback();
      tmpdest.removeCallback();
      done();
    });

    pipe.onItem({ file: tmpfile.name, dest: tmpdest.name });
  });
});
