'use strict';

const
  _ = require('lodash'),
  should = require('should'),
  tmp = require('tmp'),
  path = require('path'),
  debug = require('debug')('pipo:tests'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  pipo = require('../pipo');


describe('SQLite blobItem', function() {
  var dir;

  beforeEach(function(done) {
    dir = tmp.dirSync({ unsafeCleanup: true });
    debug('tmpdir:', dir.name);

    /* prepare a test database */
    var pipe = new pipo.SQLiteEval();
    pipe.once('end', () => done());

    pipe.onItem({
      SQLiteEvalConfig: { database: path.join(dir.name, 'test.db') },
      sql: 'CREATE TABLE test (date INTEGER PRIMARY KEY, name TEXT NOT NULL, item BLOB)'
    });
  });

  afterEach(function() {
    dir.removeCallback();
    dir = null;
  });

  function checkDB(content, done) {
    var pipe = new pipo.SQLiteIn();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.on('end', () => {
      should(items).eql(content);
      done();
    });

    pipe.onItem({
      SQLiteInConfig: {
        database: path.join(dir.name, 'test.db'), table: 'test'
      },
      where: true
    });
  }

  function testSql(input, messages, db, done) {
    var pipe = new pipo.SQLiteOut();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.once('end', () => {
      should(items).eql(messages);
      checkDB(_.isNil(db) ? messages : db, done);
    });
    pipe.onItem(input);
  }

  it('can serialize an item', function(done) {
    testSql({
      SQLiteOutConfig: {
        database: path.join(dir.name, 'test.db'),
        table: 'test'
      },
      date: 1234, name: 'toto', value: 42, another: [ 1, '2', null ]
    }, [
      { date: 1234, name: 'toto', value: 42, another: [ 1, '2', null ] }
    ], null, done);
  });

  it('does not modify row when deserialization fails', function(done) {
    testSql({
      SQLiteOutConfig: {
        database: path.join(dir.name, 'test.db'),
        table: 'test', noBlobItem: true
      },
      date: 1234, name: 'toto', item: 44
    }, [
      { date: 1234, name: 'toto', item: 44 }
    ], null, done);
  });

  it('handles an "item" property', function(done) {
    testSql({
      SQLiteOutConfig: {
        database: path.join(dir.name, 'test.db'),
        table: 'test'
      },
      date: 1234, name: 'toto', value: 42, item: 'test'
    }, [
      { date: 1234, name: 'toto', value: 42, item: 'test' }
    ], null, done);
  });
});
