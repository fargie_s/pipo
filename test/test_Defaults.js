'use strict';

const
  should = require('./should-ext'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');

describe('Defaults', function() {

  it('set a property', function(done) {
    var pipe = new pipo.Defaults();

    pipe.on('item', function(item) {
      should(item).not.have.property('toto.titi[0].tata');
      should(item).get('toto.titi[0].tata').eql(42);
      done();
    });
    pipe.onItem({
      DefaultsConfig: {
        property: 'toto.titi[0].tata', value: 42
      },
      item: 42
    });
  });

  it('does not override existing properties', function(done) {
    var pipe = new pipo.Defaults();

    pipe.on('item', function(item) {
      should(item).eql({ item: 42 });
      done();
    });
    pipe.onItem({
      DefaultsConfig: { property: 'item', value: 44 },
      item: 42
    });
  });

  it('set all properties of an item', function(done) {
    var pipe = new pipo.Defaults();

    pipe.on('item', function(item) {
      should(item).eql({
        tata: 42,
        toto: { titi: [ { tata: 44 } ] },
        tutu : 'orig'
      });
      done();
    });

    pipe.onItem({
      DefaultsConfig: {
        item: { tata: 42, 'toto.titi[0].tata': 44, tutu: 'new' }
      },
      tutu: 'orig'
    });
  });
});
