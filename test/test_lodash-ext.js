'use strict';

const
  { it, describe } = require('mocha'),
  should = require('should'),
  sinon = require('sinon'),
  _ = require('../pipo/utils/lodash-ext');

require('should-sinon');

describe('lodash-ext', function() {
  it('exports lodash', function() {
    should(_).equal(require('lodash')); // eslint-disable-line global-require
  });

  it('push element in an array', function() {
    should(_.push(null, 1)).eql([ 1 ]);

    var array = [ 1 ];
    should(_.push(array, 2)).eql([ 1, 2 ]);
    should(array).eql([ 1, 2 ]);

    /* 12 is not an array, an array with element is created */
    should(_.push(12, 1)).eql([ 1 ]);
  });

  it('bind functions', function() {
    var spy = sinon.spy();
    _.fbind(spy, 12)(13);
    spy.should.be.calledWithExactly(12, 13);
    spy.resetHistory();

    _.fbind0(spy, 12)(13);
    spy.should.be.calledWithExactly(12);
  });

  it('bind methods', function() {
    var object = { foo: _.noop };
    sinon.spy(object, 'foo');
    _.bind0(object.foo, object, 12)(13);
    object.foo.should.be.calledWithExactly(12);
    object.foo.resetHistory();

    _.mbind(object, 'foo', 12)(13);
    object.foo.should.be.calledWithExactly(12, 13);
    object.foo.resetHistory();

    _.mbind0(object, 'foo', 12)(13);
    object.foo.should.be.calledWithExactly(12);
    object.foo.resetHistory();
  });

});
