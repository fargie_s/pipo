'use strict';

const
  _ = require('lodash'),
  should = require('./should-ext'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');

describe('Range', function() {

  _.forEach([
    { config: { end: 3 }, items: [ { value: 0 }, { value: 1 }, { value: 2 } ] },
    {
      config: { start: 4, end: 1 },
      items: [ { value: 4 }, { value: 3 }, { value: 2 } ]
    },
    {
      config: { end: 3, step: -1 },
      items: [ { value: 0 }, { value: 1 }, { value: 2 } ]
    }
  ], function(test) {
    it(`generates [${test.config.start || 0}, ${test.config.end}[ range`, function(done) {
      var pipe = new pipo.Range();

      pipe
      .next(new pipo.Aggregate())
      .on('item', function(item) {
        should(item).eql({ items: test.items });
        done();
      });
      pipe.onItem({ RangeConfig: test.config });
    });
  });

  it('generates a range with floats', function(done) {
    var pipe = new pipo.Range();

    pipe
    .next(new pipo.Aggregate())
    .on('item', function(item) {
      should(item).have.property('items').size(3);
      should(item.items[0].i).approximately(0.1, 0.001);
      should(item.items[1].i).approximately(0.2, 0.001);
      should(item.items[2].i).approximately(0.3, 0.001);
      done();
    });
    pipe.onItem(
      { RangeConfig: { start: 0.10, step: 0.10, end: 0.40, property: 'i' } });

  });
});
