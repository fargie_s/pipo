'use strict';

const
  _ = require('lodash'),
  sinon = require('sinon'),
  should = require('./should-ext'),
  nodemailer = require('nodemailer'),
  { describe, it, afterEach, beforeEach } = require('mocha'),
  { dbgStr } = require('./Helpers');

const
  pipo = require('../pipo');

describe('MailOut', function() {
  var sandbox = sinon.createSandbox();
  var mock;

  beforeEach(function() {
    sandbox.stub(nodemailer, "createTransport")
    .callsFake(function() {
      var ret  = { sendMail: _.noop };
      mock = sandbox.stub(ret, 'sendMail').callsArg(1);
      return ret;
    });
  });

  afterEach(function() {
    sandbox.restore();
  });

  _.forEach([
    {
      mail: { from: "Pipo <piper.pipo@gmail.com>",
        subject: "pipo the piper", to: 'titi', text: 'this is a text' },
      item: { to: 'titi', text: 'this is a text' }
    },
    {
      mail: { from: "toto", subject: "sub", to: 'titi', text: 'this text' },
      item: { from: "toto", subject: "sub", to: 'titi', text: 'this text' }
    },
    {
      mail: { from: "toto", subject: "sub", to: 'titi', text: 'this text' },
      item: { MailOutConfig: { from: "toto", subject: "sub", to: 'titi' },
        text: 'this text' }
    }
  ], function(test) {
    it(`sends a mail ${dbgStr(test.mail)}`, function(done) {
      var pipe = new pipo.MailOut();
      pipe.once('end', function() {
        should(mock).be.calledWith(test.mail).calledOnce();
        done();
      });
      pipe.onItem({ MailOutConfig: { server: 'smtps://smtp.gmail.com' } });
      pipe.onItem(test.item);
    });
  });

  it('sends item as an attachment', function(done) {
    var pipe = new pipo.MailOut();
    pipe.once('end', function() {
      should(mock).be.calledOnce();
      should(mock.args[0]).get('[0].attachments[0]')
      .eql({
        contentType: 'application/json', filename: 'item.json',
        content: '{"value":42}'
      });
      done();
    });
    pipe.onItem({ MailOutConfig: { server: 'smtps://smtp.gmail.com' } });
    pipe.onItem({ to: 'me@here.com', text: 'test', value: 42 });
  });

  it('sends an attachment', function(done) {
    var pipe = new pipo.MailOut();
    pipe.once('end', function() {
      should(mock).be.calledOnce();
      should(mock.args[0]).get('[0].attachments')
      .size(2).get('[0]')
      .eql({
        contentType: 'text/plain', filename: 'test.txt',
        content: 'test'
      });
      done();
    });
    pipe.onItem({ MailOutConfig: { server: 'smtps://smtp.gmail.com' } });
    pipe.onItem({ to: 'me@here.com', text: 'test', value: 42,
      attachments: [{
        filename: 'test.txt',
        contentType: 'text/plain',
        content: 'test'
      }]
    });
  });

  it('sends item only when requested', function(done) {
    var pipe = new pipo.MailOut();
    pipe.once('end', function() {
      should(mock).be.calledOnce();
      should(mock.args[0]).not.get('[0].attachments');
      done();
    });
    pipe.onItem({ MailOutConfig: {
      server: 'smtps://smtp.gmail.com',
      itemName: null
    } });
    pipe.onItem({ to: 'me@here.com', text: 'test', value: 42 });
  });

  it('sends error on failure', function(done) {
    nodemailer.createTransport.callsFake(function() {
      var ret  = { sendMail: _.noop };
      mock = sandbox.stub(ret, 'sendMail')
      .callsArgWith(1, 'this is an error');
      return ret;
    });
    var pipe = new pipo.MailOut();
    pipe.once('item', function(item) {
      should(mock).be.calledOnce();
      should(item).have.property('errorString',
        'Failed to send mail: this is an error');
      done();
    });
    pipe.onItem({ MailOutConfig: { server: 'smtps://smtp.gmail.com' } });
    pipe.onItem({ to: 'me@here.com', text: 'test' });
  });

  it('overrides transport', function(done) {
    var pipe = new pipo.MailOut();
    pipe.once('end', function() {
      var transport = nodemailer.createTransport;
      transport.should.be.calledTwice();
      should(transport.getCall(0)).property('args')
      .eql([ 'smtps://smtp.kmail.com' ]);
      should(transport.getCall(1)).property('args')
      .eql([ 'smtps://smtp.gmail.com' ]);
      done();
    });
    pipe.onItem({ MailOutConfig: { server: 'smtps://smtp.kmail.com' } });
    pipe.onItem({ to: 'me@here.com', text: 'test' });
    pipe.onItem({ MailOutConfig: { server: 'smtps://smtp.gmail.com' } });
    pipe.onItem({ to: 'me@here.com', text: 'test' });
  });
});
