'use strict';

const
{ describe, it, afterEach, beforeEach } = require('mocha'),
  q = require('q'),
  tmp = require('tmp');

const
  Applet = require('../pipo/utils/Applet');

describe('Applet', function() {
  var tmpdir;

  beforeEach(function() {
    tmpdir = tmp.dirSync({ unsafeCleanup: true });
  });

  afterEach(function() {
    tmpdir.removeCallback();
  });

  it('displays help', function() {
    var app = new Applet([ 'node', 'applet', '--help' ]);
    return q().then(() => {
      return app.run();
    });
  });
});
