'use strict';

const
  should = require('should'),
  { describe, it, before, after } = require('mocha'),
  moment = require('moment-timezone'),
  sinon = require('sinon'),
  _ = require('lodash');

const
  pipo = require('../pipo');


describe('Date', function() {
  var sandbox;

  before(function() {
    sandbox = sinon.createSandbox();
    sandbox.stub(moment, 'suppressDeprecationWarnings').value(true);
  });

  after(function() {
    sandbox.restore();
  });

  var pipe = new pipo.Date();

  _.forEach([
      { in: 1505939177, inFormat: 'X', outFormat: 'YYYY-MM-DD', out: '2017-09-20' },
      { in: '2017-09-20', inFormat: 'YYYY-MM-DD', outFormat: 'YYYY-MM-DD', out: '2017-09-20' },
      { in: '2017-09-20', inFormat: 'YYYY-MM-DD', outFormat: 'X', out: '1505865600' }
    ],
    function(sample) {
      it(`format "${sample.in}"`, function(done) {
        pipe.once('item', (item) => {
          should(item).have.property('date').eql(sample.out);
          done();
        });
        pipe.onItem({
          DateConfig: {
            property: "date",
            inFormat: sample.inFormat,
            outFormat: sample.outFormat
          },
          date: sample.in
        });
      });
    }
  );

  it('fails to parse corrupted date', function(done) {
    pipe.once('item', (item) => {
      should(item).have.property('errorString');
      done();
    });
    pipe.onItem({
      DateConfig: { property: "date", inFormat: null, outFormat: null },
      date: "not a date"
    });
  });
});
