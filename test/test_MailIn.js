'use strict';

const
  EventEmitter = require('events'),
  fs = require('fs'),
  _ = require('lodash'),
  sinon = require('sinon'),
  should = require('./should-ext'),
  { describe, it, afterEach, beforeEach } = require('mocha');

const
  MailIn = require('../pipo/input/MailIn'),
  pipo = require('../pipo');

class ImapStub extends EventEmitter {
  constructor() {
    super();
    this._mails = [ 'sample_mail.txt' ];
  }
  connect() {
    this.emit('ready');
  }
  end() {}
  openBox(mbox, mode, cb) {
    cb(null);
  }
  search(criteria, cb) {
    cb(null, this._mails);
  }
  fetch(list) {
    return new ImapFetchStub(list);
  }
  addFlags(mid, flags, cb) {
    cb(null);
  }
}

class ImapMessageStub extends EventEmitter {
  constructor(msg) {
    super();
    _.defer(() => {
      this.emit('attributes', { uid: 0 });
      this.emit('body', fs.createReadStream(`${__dirname}/data/${msg}`));
      this.emit('end');
    });
  }
}

class ImapFetchStub extends EventEmitter {
  constructor(list) {
    super();
    _.defer(() => {
      _.forEach(list, (msg) => {
        this.emit('message', new ImapMessageStub(msg));
      });
      this.emit('end');
    });
  }
}

describe('MailIn', function() {
  var sandbox = sinon.createSandbox();

  beforeEach(function() {
    sandbox.stub(MailIn, 'Imap')
    .callsFake(function() {
      return new ImapStub();
    });
  });

  afterEach(function() {
    sandbox.restore();
  });

  it('sends an error on failure', function(done) {
    var mail = new pipo.MailIn();
    var received = false;

    sandbox.stub(ImapStub.prototype, 'connect')
    .callsFake(function() {
      this.emit('error', 'this is an error');
    });
    mail.once('item', function(e) {
      should(e).have.property('errorString');
      received = true;
    });
    mail.once('end', function() {
      should(received).be.true();
      done();
    });
    mail.onItem({ MailInConfig: { server: 'imaps://gmail.com' } });
  });

  it('retrieves mails', function(done) {
    var mail = new pipo.MailIn();
    var received = false;
    mail.once('item', _.partial(_.defer,
      function(item) {
      should(item).have.property('value', 42);
      received = true;
    }));
    mail.once('end', function() {
      should(received).be.true();
      done();
    });
    mail.onItem({ MailInConfig: { server: 'imaps://gmail.com' } });
  });

  it('retrieves raw mails', function(done) {
    var mail = new pipo.MailIn();
    var received = false;

    mail.once('item', _.partial(_.defer,
      function(item) {
      should(item).containDeep({
        from: { address: 'piper.pipo@gmail.com', name: 'Pipo' },
        to: 'piper.pipo@gmail.com',
        messageId: '<328190e6-12dc-62fb-6c47-ac9f6ff24306@gmail.com>',
        subject: 'pipo the piper',
        text: 'sample text',
        attachments: [{
          content: 'eyJ2YWx1ZSI6NDJ9', contentType: 'application/json',
          filename: 'item.json'
        }]
      });
      received = true;
    }));
    mail.once('end', function() {
      should(received).be.true();
      done();
    });
    mail.onItem({ MailInConfig: { raw: true, server: 'imaps://gmail.com' } });
  });
});
