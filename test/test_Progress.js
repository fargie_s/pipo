'use strict';

const
  { describe, it, beforeEach, afterEach } = require('mocha'),
  sinon = require('sinon'),
  should = require('should'),
  Progress = require('../pipo/output/Progress'),
  pipo = require('../pipo');

require('should-sinon');

describe('Progress', function() {
  var sandbox;

  beforeEach(function() {
    sandbox = sinon.createSandbox();
  });

  afterEach(function() {
    sandbox.restore();
    sandbox = null;
  });

  it('displays text', function(done) {
    var pipe = new pipo.Progress();

    pipe._ora();
    should(Progress).have.property('_ora');
    sandbox.stub(Progress._ora);

    pipe.once('end', () => {
      should(Progress._ora.stop).be.calledOnce();
      done();
    });

    pipe.onItem({ text: 'test', warn: 'warning', info: 'information' });
    should(Progress._ora.start).be.calledOnce();
    should(Progress._ora.text).eql('test');
    should(Progress._ora.warn).be.calledWith('warning');
    should(Progress._ora.info).be.calledWith('information');

    pipe.end();
  });

  it('displays errors', function() {
    var pipe = new pipo.Progress();

    pipe._ora();
    should(Progress).have.property('_ora');
    sandbox.stub(Progress._ora);

    pipe.onItem({ errorString: 'test' });
    should(Progress._ora.warn).be.calledOnce();

    pipe.onItem({ ProgressConfig: { warnOnError: false } });
    pipe.onItem({ errorString: 'test' });
    should(Progress._ora.warn).be.calledOnce();

    pipe.onItem({ ProgressConfig: { warnOnError: true } });
    pipe.onItem({ errorString: 'test' });
    should(Progress._ora.warn).be.calledTwice();
  });

  it('persists last message', function(done) {
    var pipe = new pipo.Progress();

    pipe._ora();
    should(Progress).have.property('_ora');
    sandbox.stub(Progress._ora);

    pipe.once('end', () => {
      should(Progress._ora.stopAndPersist).be.calledOnce();
      /* initial value should be restored */
      should(Progress._persistOnEnd).be.eql(false);
      done();
    });

    pipe.onItem({
      text: 'test',
      ProgressConfig: { persistOnEnd: true }
    });
    should(Progress._ora.start).be.calledOnce();
    pipe.end();
  });

  it('prints last message', function(done) {
    var pipe = new pipo.Progress();

    pipe._ora();
    should(Progress).have.property('_ora');
    sandbox.stub(Progress._ora);

    pipe.once('end', () => {
      should(Progress._ora.succeed).be.calledWith('last message');
      /* initial value should be restored */
      should(Progress._persistOnEnd).be.eql(false);
      done();
    });

    pipe.onItem({
      text: 'test',
      ProgressConfig: { persistOnEnd: 'last message' }
    });
    should(Progress._ora.start).be.calledOnce();
    pipe.end();
  });
});
