'use strict';

const
  should = require('should'),
  tmp = require('tmp'),
  path = require('path'),
  debug = require('debug')('pipo:tests'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  pipo = require('../pipo');


describe('SQLiteIn', function() {
  var dir;

  beforeEach(function(done) {
    dir = tmp.dirSync({ unsafeCleanup: true });
    debug('tmpdir:', dir.name);

    /* prepare a test database */
    var pipe = new pipo.SQLiteEval();
    pipe.once('end', () => done());

    pipe.onItem({
      SQLiteEvalConfig: { database: path.join(dir.name, 'test.db') },
      sql: 'CREATE TABLE test (date INTEGER PRIMARY KEY, name TEXT)'
    });
    pipe.onItem({
      sql: 'INSERT INTO test VALUES (:date, :name)', date: 1234, name: 'toto'
    });
    pipe.onItem({
      sql: 'INSERT INTO test VALUES (:date, :name)', date: 1235, name: 'toto'
    });
  });

  afterEach(function() {
    dir.removeCallback();
    dir = null;
  });

  it('can find an item in a database', function(done) {
    var pipe = new pipo.SQLiteIn();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.on('end', () => {
      should(items).eql([ { date: 1234, name: 'toto' } ]);
      done();
    });

    pipe.onItem({
      SQLiteInConfig: {
        database: path.join(dir.name, 'test.db'),
        table: 'test'
      },
      where: 'date = :date',
      date: 1234
    });
  });

  it('can dump the complete table', function(done) {
    var pipe = new pipo.SQLiteIn();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.on('end', () => {
      should(items).eql([
        { date: 1234, name: 'toto' },
        { date: 1235, name: 'toto' },
      ]);
      done();
    });

    pipe.onItem({
      SQLiteInConfig: { database: path.join(dir.name, 'test.db') },
      where: 'true', table: 'test'
    });
  });

  function testSql(input, results, done) {
    var pipe = new pipo.SQLiteIn();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.once('end', () => {
      should(items).eql(results);
      done();
    });
    pipe.onItem(input);
  }

  it('fails on invalid where clause', function(done) {
    testSql(
      {
        SQLiteInConfig: { database: path.join(dir.name, 'test.db') },
        table: 'test', where: 'INVALID STATEMENT'
      },
      [ { errorString: 'near "STATEMENT": syntax error' } ],
      done
    );
  });

  it('fails to create an invalid database', function(done) {
    testSql(
      {
        SQLiteInConfig: { database: '' },
        table: 'test', where: 'date = 1234'
      },
      [ { errorString: 'A database filename cannot be an empty string' } ],
      done
    );
  });

  it('fails on missing table information', function(done) {
    testSql(
      {
        SQLiteInConfig: { database: path.join(dir.name, 'test.db') },
        where: 'date = 1234'
      },
      [ { errorString: 'SQLite table not set' } ],
      done
    );
  });
});
