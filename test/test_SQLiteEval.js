'use strict';

const
  should = require('should'),
  tmp = require('tmp'),
  path = require('path'),
  debug = require('debug')('pipo:tests'),
  { describe, it, beforeEach, afterEach } = require('mocha');

const
  pipo = require('../pipo');


describe('SQLiteEval', function() {
  var dir;

  beforeEach(function() {
    dir = tmp.dirSync({ unsafeCleanup: true });
    debug('tmpdir:', dir.name);
  });

  afterEach(function() {
    dir.removeCallback();
    dir = null;
  });

  it('can run an sql statement', function(done) {
    var pipe = new pipo.SQLiteEval();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.on('end', () => {
      should(items).eql([
        { date: 1234, name: 'toto' }
      ]);
      done();
    });

    pipe.onItem({
      SQLiteEvalConfig: { database: path.join(dir.name, 'test.db') },
      sql: 'CREATE TABLE test (date INTEGER PRIMARY KEY, name TEXT)'
    });
    pipe.onItem({
      sql: 'INSERT INTO test VALUES (:date, :name)',
      date: 1234, name: 'toto'
    });
    pipe.onItem({ sql: 'SELECT * FROM test' });
  });

  it('can run several sql statements in a row', function(done) {
    var pipe = new pipo.SQLiteEval();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.on('end', () => {
      should(items).eql([ { date: 1234 } ]);
      done();
    });

    pipe.onItem({
      SQLiteEvalConfig: { database: path.join(dir.name, 'test.db') },
      date: 1234, name: 'toto',
      sql: [
        'CREATE TABLE test (date INTEGER PRIMARY KEY, name TEXT)',
        'INSERT INTO test VALUES (:date, :name)',
        'SELECT date FROM test'
      ]
    });
  });

  function testSql(input, results, done) {
    var pipe = new pipo.SQLiteEval();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.once('end', () => {
      should(items).eql(results);
      done();
    });
    pipe.onItem(input);
  }

  it('fails on invalid sql', function(done) {
    testSql(
      {
        SQLiteEvalConfig: { database: path.join(dir.name, 'test.db') },
        sql: 'INVALID STATEMENT'
      },
      [ { errorString: 'near "INVALID": syntax error' } ],
      done
    );
  });

  it('fails to create an invalid database', function(done) {
    testSql(
      {
        SQLiteEvalConfig: { database: '' },
        sql: 'CREATE TABLE test (date INTEGER PRIMARY KEY)'
      },
      [ { errorString: 'A database filename cannot be an empty string' } ],
      done
    );
  });

  it('fails to run sql when database is not set', function(done) {
    testSql(
      {
        sql: 'CREATE TABLE test (date INTEGER PRIMARY KEY)'
      },
      [ { errorString: 'SQLite database not set' } ],
      done
    );
  });
});
