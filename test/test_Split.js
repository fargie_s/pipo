'use strict';

const
  should = require('should'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');


describe('Split', function() {

  it('split items', function(done) {
    var pipe = new pipo.Split();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.once('end', () => {
      should(items).eql([
        { test: 42 },
        { titi: 44 },
        { toto: 44 }
      ]);
      done();
    });
    pipe.onItem({
      items: [ { test: 42 }, { titi: 44 } ],
      toto: 44
    });
  });

  it('split properties', function(done) {
    var pipe = new pipo.Split();
    var items = [];

    pipe.on('item', (item) => items.push(item));
    pipe.once('end', () => {
      should(items).eql([
        { "test": 42, "titi": 43 },
        { "test": 45, "titi": 43 }
      ]);
      done();
    });
    pipe.onItem({
      SplitConfig: { property: "test" },
      test: [ 42, 45 ],
      titi: 43
    });
  });
});
