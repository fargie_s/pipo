'use strict';

const
  {describe, it} = require('mocha'),
  should = require('should');

const
  pipo = require('../pipo'),
  DataIn = require('../pipo/DataIn');

describe('DataIn', function() {
  it('parses json', function(done) {
    var dataIn = new DataIn();
    var accu = new pipo.Aggregate();
    dataIn.next(accu);

    accu.once('item', function(item) {
      should(item).eql({
        items: [
          { sample: "{{{{\"" },
          { partial: "item" }
        ]
      });
      done();
    });
    dataIn.add(new Buffer('  { "sample": "{{{{\\"" }'));
    dataIn.add(new Buffer('{ "par'));
    dataIn.add(new Buffer('tial": '));
    dataIn.add(new Buffer(' "item" }'));
    accu.end(0);
  });

  it('parses all json types', function(done) {
    var dataIn = new DataIn();
    var testItem = {
      array: [ { object: 'test' }, [ ], 12, true, false, "test" ],
      bool: true,
      sub: { }
    };

    dataIn.once('item', function(item) {
      should(item).eql(testItem);
      done();
    });
    dataIn.add(new Buffer(JSON.stringify(testItem)));
  });

  it('can resize internal buffer', function(done) {
    var dataIn = new DataIn(2);

    dataIn.once('item', function(item) {
      should(item).eql({ test: 42 });
      done();
    });
    dataIn.add(new Buffer('{ "test": 42 }'));
  });

  it('sends an error on invalid input', function(done) {
    var dataIn = new DataIn();

    dataIn.once('item', function(item) {
      should(item).have.property('errorString');
      done();
    });
    dataIn.add(new Buffer('{ invalid }'));
  });
});
