'use strict';

const
  should = require('should'),
  { describe, it } = require('mocha'),
  { dbgStr } = require('./Helpers'),
  _ = require('lodash');

const
  pipo = require('../pipo');


describe('JSEval', function() {

  _.forEach([
      { in: { expr: '1 + 1' }, out: { result: 2 } },
      { in: { expr: '1 + item.a', a: 1 }, out: { result: 2, a: 1 }},
      { in: { expr: 'item.a += 1', a: 1 }, out: { a: 2 }}
    ],
    function(sample) {
      it(`eval ${dbgStr(sample.in)} -> ${dbgStr(sample.out)}`, function(done) {
        var pipe = new pipo.JSEval();
        pipe.once('item', (item) => {
          should(item).containEql(sample.out);
          should(item).not.have.property('expr');
          done();
        });
        pipe.onItem(sample.in);
      });
  });

  it('throws errors', function(done) {
    var pipe = new pipo.JSEval();

    pipe.once('item', (item) => {
      should(item).have.property('errorString');
      done();
    });
    pipe.onItem({ expr: '1 + a' });
  });

  it('uses configuration', function(done) {
    var pipe = new pipo.JSEval();

    pipe.next(new pipo.Aggregate())
    .once('item', (item) => {
      should(item).have.property('items')
      .eql([ { b: 42 }, { a: 2, ret: 3} ]);
      done();
    });
    pipe.onItem({ JSEvalConfig: { expr: '1+item.a || undefined', property: 'ret' } });
    pipe.onItem({ b: 42 });
    pipe.onItem({ a: 2 });
    pipe.end();
  });

  it('let errors pass', function(done) {
    var pipe = new pipo.JSEval();

    pipe.once('item', (item) => {
      should(item).eql({ errorString: 'sample error' });
      done();
    });
    pipe.onItem({ JSEvalConfig: { expr: '1', property: 'ret' } });
    pipe.onItem({ errorString: 'sample error' });
    pipe.end();
  });
});
