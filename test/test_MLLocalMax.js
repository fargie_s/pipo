'use strict';

const
  should = require('./should-ext'),
  { describe, it } = require('mocha');

const
  pipo = require('../pipo');

describe('MLLocalMax', function() {

  it('finds a simple maximum', function(done) {
    var pipe = new pipo.MLLocalMax();

    pipe.on('item', function(item) {
      should(item).have.property('x').approximately(10, 0.1);

      /* resulting item should have a score value */
      should(item).have.property('score').approximately(10, 0.1);

      /* y is unrelated but should be kept */
      should(item).have.property('y').eql(12);
      done();
    });
    pipe.onItem({
      MLLocalMaxConfig: {
        pipe: {
          pipe: 'MathEval',
          MathEvalConfig: { expr: "10 - abs(10 - x)", property: 'score' }
        },
        var: { x: { step: 0.1 } }
      },
      x: 0, y: 12
    });
  });

  it('respects min and max', function(done) {
    this.timeout(3000);
    var pipe = new pipo.MLLocalMax();

    pipe.on('item', function(item) {
      should(item).have.property('x').approximately(10, 0.1);
      should(item).have.property('y').approximately(-10, 0.1);
      done();
    });
    pipe.onItem({
      MLLocalMaxConfig: {
        pipe: {
          pipe: 'MathEval',
          MathEvalConfig: { expr: "x - y", property: 'score' }
        },
        var: {
          x: { step: 0.1, min: -10, max: +10 },
          y: { step: 0.1, min: -10, max: +10 }
        }
      },
      x: 0, y: 0
    });
  });

  it('forwards errors', function(done) {
    var pipe = new pipo.MLLocalMax();
    pipe.once('item', function(item) {
      should(item).have.property('errorString');
      done();
    });
    pipe.onItem({
      MLLocalMaxConfig: {
        pipe: {
          pipe: 'MathEval',
          MathEvalConfig: { property: 'score' }
        },
        var: { }
      },
      expr: "x - y",
      x: 0 // y is missing on purpose to trigger an error
    });
  });

  it('knows which generation is running', function(done) {
    var pipe = new pipo.MLLocalMax();
    pipe.once('item', function(item) {
      should(item).have.property('score').eql(10);
      done();
    });
    pipe.onItem({
      MLLocalMaxConfig: {
        maxGen: 10,
        pipe: {
          pipe: 'MathEval',
          MathEvalConfig: {
            expr: 'generation',
            property: 'score'
          }
        },
        var: { }
      },
      x: 0
    });
  });
});
