'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2016-11-27T17:19:28+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  htmltidy = require('htmltidy'),
  _ = require('lodash'),
  q = require('q'),
  Item = require('../Item'),
  PipeElement = require('../PipeElement');

const s_nsRe = /<[^\s]+\s+(?:[^\s=>]+="[^"]*"\s+)*[^\s=>]+(:)/g;

/**
 * @module HTMLToXML
 * @description Convert an HTML document to XML
 *
 * ### Configuration
 * | Name             | Type    | Default | Description                  |
 * | :--------------- | :------ | :------ | :--------------------------- |
 * | [`noNamespaces`] | boolean | true    | Flatten namespaces           |
 * | [`property`]     | string  | null    | Property to transform to XML |
 *
 * ### Items
 * | Name       | Type           | Description              |
 * | :--------- | :------------- | :----------------------- |
 * | `html`     | string         | HTML document to convert |
 *
 * ### Details
 * If *property* is null, then *html* will be used as input property and output
 * will be set to *xml* property.
 *
 * Uses [htmltidy](http://www.html-tidy.org/) to convert documents.
 */
class HTMLToXML extends PipeElement {
  constructor() {
    super();
    this.noNamespaces = true;
    this.property = null;
  }

  onItem(item) {
    super.onItem(item);

    var prop = _.defaultTo(this.property, 'html');
    if (_.has(item, prop)) {
      this.ref();
      this._tidyfy(Item.take(item, prop))
      .then(this._removeNs.bind(this))
      .then(
        (xml) => {
          _.set(item, _.defaultTo(this.property, 'xml'), xml);
          this.emitItem(item);
        },
        (err) => {
          this.error(err.toString());
        }
      )
      .finally(this.unref.bind(this));
    }
    else {
      this.emitItem(item);
    }
  }

  _tidyfy(html) {
    var opts = {
      "output-xml": true,
      "quote-nbsp": false,
      "quiet": true,
      "input-encoding": "utf-8",
      "output-encoding": "utf-8",
      "force-output": true,
      "clean": true
    };
    return q.nfcall(htmltidy.tidy, html, opts);
  }

  _removeNs(xml) {
    if (!this.noNamespaces) {
      return xml;
    }

    var isMatch = true;
    function reMatch(match) {
      isMatch = true;
      return match.slice(0, match.length - 1) + '_';
    }

    while (isMatch) {
      isMatch = false;
      xml = xml.replace(s_nsRe, reMatch);
    }
    return xml.replace(/xmlns=/, 'xmlns_=');
  }
}

module.exports = HTMLToXML;
