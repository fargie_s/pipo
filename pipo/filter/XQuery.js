'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2016-11-29T09:17:15+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  libxml = require('libxmljs'),
  _ = require('lodash'),
  debug = require('debug')('pipo:filter'),
  PipeElement = require('../PipeElement'),
  Item = require('../Item');

/**
 * @module XQuery
 * @description Run an XQuery/XPath query on an xml document
 *
 * ### Configuration
 * | Name         | Type           | Default | Description                 |
 * | :----------- | :------------- | :------ | :-------------------------- |
 * | `query`      | string         | null    | The main XQuert query       |
 * | `subQueries` | object         | null    | Sub-queries                 |
 * | `property`   | string or path | 'xml'   | Input XML document property |
 * | `trim`       | boolean        | false   | Trim output                 |
 *
 * ### Details
 * The *subQueries* is an object that maps property-names -> XPath selectors,
 * properties with the same name will be set in output object.
 *
 * @example
 * {
 *   "pipe": "XQuery",
 *   "xml": "<div><div class=\"link_list\"><ul><li class=\"test seme\"><a href='http://test' title='42' /></li></ul></div></div>",
 *   "query": "//div[@class='link_list']//li[contains(@class, 'seme')]/a",
 *   "subQueries": {
 *     "href": "/@href",
 *     "title": "/@title"
 *   }
 * }
 * ===
 * { "href": "http://test", "title": "42" }
 *
 */
class XQuery extends PipeElement {
  constructor() {
    super();
    this.trim = false;
    this.subQueries = null;
    this.query = null;
  }

  onItem(item) {
    super.onItem(item);

    if ('xml' in item) {
      let query = Item.take(item, 'query', this.query);
      let subQueries = _.clone(Item.take(item, 'subQueries', this.subQueries));
      let xml = Item.take(item, 'xml');

      try {
        _.forOwn(subQueries, function(value, key) {
          if (!value.startsWith('./')) {
            subQueries[key] = './' + value;
          }
        });

        if (!query || !subQueries) {
          throw "\"query\" and \"subQueries\" are required in XQuery elements";
        }
        let doc = libxml.parseXml(xml);
        let nodes = doc.find(query);

        debug('%i nodes selected', nodes.length);
        _.forIn(nodes, (node) => {
          let out = _.cloneDeep(item);
          _.forOwn(subQueries, (value, key) => {
            let ret = node.get(value);
            ret = (_.isNil(ret.value)) ? ret.text() : ret.value();
            out[key] = (this.trim) ? _.trim(ret) : ret;
          });
          this.emit('item', out); // FIXME: seems to be directly connected to callback
        });
      } catch (e) {
        debug('XQuery error', e);
        this.error(e.toString());
        return;
      }
    }
    else {
      this.emitItem(item);
    }
  }
}

module.exports = XQuery;
