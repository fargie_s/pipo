/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2017-09-20T22:21:24+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  moment = require('moment-timezone'),
  PipeElement = require('../PipeElement');

/**
 * @module Date
 * @description Date conversions element
 *
 * ### Configuration
 * | Name          | Type           | Default | Description         |
 * | :------------ | :------------- | :------ | :------------------ |
 * | `property`    | string         | null    | Property to convert |
 * | [`outFormat`] | string         | null    | Date output format  |
 * | [`inFormat`]  | string         | null    | Date input format   |
 * | [`inTZ`]      | string         | 'UTC'   | Date input timezone |
 * | [`outTZ`]     | string         | 'UTC'   | Date output format  |
 *
 * ### Details
 *
 * If inFormat/outFormat is null, dates will be parsed with
 * [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601),
 * [RFC 2822](https://tools.ietf.org/html/rfc2822#section-3.3) or Javascript
 * Date(string) format.
 *
 * See [Moment](https://momentjs.com/docs/#/parsing/string/) for details about date parsing.
 */
/* Native language parsing dates */
class Date extends PipeElement {
  constructor() {
    super();
    this.property = null;
    this.outFormat = null;
    this.inFormat = null;
    this.inTZ = 'UTC';
    this.outTZ = 'UTC';
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isNil(this.property) && _.has(item, this.property)) {
      var date = moment
        .tz(item[this.property], this.inFormat, this.inTZ)
        .tz(this.outTZ);
      if (date.isValid()) {
        item[this.property] = date.format(this.outFormat);
        this.emitItem(item);
      }
      else {
        this.error(`Invalid date or format for "${item[this.property]}"`);
      }
    }
    else {
      this.emitItem(item);
    }
  }
}

module.exports = Date;
