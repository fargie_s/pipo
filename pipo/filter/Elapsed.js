'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-08-31T10:14:28+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:elapsed'),
  process = require('process'),
  PipeElement = require('../PipeElement');

/**
 * @module Elapsed
 * @description Measure elapsed time since pipeline creation
 *
 *  * ### Configuration
 * | Name         | Type   | Default   | Description     |
 * | :----------- | :----- | :-------- | :-------------- |
 * | `property`   | string | 'elapsed' | output property |
 *
 * ### Details
 *
 * Elapsed time is returned in milliseconds.
 *
 * Use a null or empty property to disable the elapsed counter.
 *
 * @example
 * {
 *   "pipe": "Elapsed",
 *   "text": "Hello, world !",
 * }
 * ===
 * {
 *   "text": "Hello, world !",
 *   "elapsed": 100
 * }
 */
class Elapsed extends PipeElement {
  constructor() {
    super();
    this.property = 'elapsed';
    this._time = process.hrtime();
    debug('starting to measure time');
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isEmpty(item) && !_.isEmpty(this.property)) {
      let delta = process.hrtime(this._time);
      delta = delta[0] * 1e3 + Math.round(delta[1] / 1e6);

      _.set(item, this.property, delta);
      debug('elapsed:', delta);
    }
    this.emitItem(item);
  }
}

module.exports = Elapsed;
