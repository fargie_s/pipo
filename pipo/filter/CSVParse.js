'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-04-15T15:00:00+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  Parse = require('csv-parse'),
  debug = require('debug')('pipo:csv'),
  _ = require('lodash'),
  Item = require('../Item'),
  PipeElement = require('../PipeElement');

/**
 * @module CSVParse
 * @description Parse CSV data into Json objects
 *
 * ### Configuration
 * | Name             | Type           | Default | Description                      |
 * | :--------------- | :------------- | :------ | :------------------------------- |
 * | [`property`]     | string or path | 'csv'   | Input property name              |
 * | [`columns`]      | string array   | null    | Columns description              |
 * | [`delimiter`]    | string         | ','     | Single character field delimiter |
 *
 * ### Details
 *
 * If *columns* is set to true (special value), then column names will be
 * automatically detected using first CSV line.
 *
 * If *columns* are unspecified (null), items with a *row* array property will
 * be sent.
 *
 * @example
 * // not using columns
 * {
 *   "pipe": "CSVParse"
 * }
 * { "csv": "12,13,14\n15,16,blue" }
 * ===
 * { "row": [ 12, 13, 14 ] }
 * { "row": [ 15, 16, "blue" ] }
 *
 * @example
 * // using automatic columns names
 * {
 *   "pipe": "CSVParse"
 *   "CSVParseConfig": { "columns": true }
 * }
 * { "csv": "first,last,age\nBob,Sponge,14" }
 * ===
 * {
 *   "first": "Bob",
 *   "last": "Sponge",
 *   "age": 14
 * }
 */
class CSVParse extends PipeElement {
  constructor() {
    super();
    this.property = 'csv';
    this.columns = null;
    this.delimiter = ',';
  }

  onItem(item) {
    super.onItem(item);

    if (_.has(item, this.property)) {
      let csv = Item.take(item, this.property);
      this.ref();
      var parser = new Parse({
        cast: true, cast_date: true,
        columns: this.columns, delimiter: this.delimiter
      });
      parser.on('readable', () => {
        for (var rec = parser.read(); !_.isNil(rec); rec = parser.read()) {
          this.emitItem(_.isArray(rec) ? { row: rec } : rec);
        }
      });
      parser.once('error', (err) => {
        this.error(err.message);
      });
      parser.once('end', () => {
        debug('csv parsing finished');
        parser.removeAllListeners('readable');
        parser.removeAllListeners('error');
        this.unref();
      });
      parser.write(csv);
      parser.end();
    }
    this.emitItem(item);
  }
}

module.exports = CSVParse;
