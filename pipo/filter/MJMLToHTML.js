'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-08-11T09:54:28+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  mjml = require('mjml'),
  debug = require('debug')('pipo:mjml'),
  _ = require('lodash'),
  Item = require('../Item'),
  PipeElement = require('../PipeElement');

/**
 * @module MJMLToHTML
 * @description Convert an MJML-HTML document to standalone mail
 *
 * ### Configuration
 * | Name             | Type    | Default | Description         |
 * | :--------------- | :------ | :------ | :------------------ |
 * | [`beautify`]     | boolean | false   | Beautify the output |
 * | [`minify`]       | boolean | true    | Minify the output   |
 * | [`keepComments`] | boolean | false   | Keep comments       |
 * | [`property`]     | string  | null    | Property to convert |
 *
 * ### Items
 * | Name       | Type           | Description              |
 * | :--------- | :------------- | :----------------------- |
 * | `mjml`     | string         | MJML document to convert |
 *
 * ### Details
 * If *property* is null, then *mjml* will be used as input property and output
 * will be set to *html* property.
 *
 * Uses [MJML](http://mjml.io/) to convert documents.
 */
class MJMLToHTML extends PipeElement {
  constructor() {
    super();
    this.beautify = false;
    this.minify = true;
    this.keepComments = false;
    this.property = null;
  }

  onItem(item) {
    super.onItem(item);

    var prop = _.defaultTo(this.property, 'mjml');
    if (_.has(item, prop)) {
      debug('converting "%s"', prop);
      var html;
      try {
        html = mjml(Item.take(item, prop), {
          beautify: this.beautify, minify: this.minify,
          keepComments: this.keepComments
        });
      }
      catch (e) {
        this.error(e);
      }
      _.forEach(_.get(html, 'errors'), (e) => this.error(e));
      _.set(item, _.defaultTo(this.property, 'html'), _.get(html, 'html', ''));
    }
    this.emitItem(item);
  }

  error(e) {
    e = _.get(e, 'message', e);
    debug('error:', e);
    super.error(e);
  }
}

module.exports = MJMLToHTML;
