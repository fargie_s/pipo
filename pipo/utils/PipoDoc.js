const
  path = require('path'),
  fs = require('fs'),
  _ = require('lodash'),
  debug = require('debug')('pipo:doc'),
  jsdoc2md = require('jsdoc-to-markdown');

require('colors');

function formatter(line, r) {
  return line.replace(r.re, r.cb);
}

class PipoDoc {
  constructor(fd) {
    this.fd = fd;
  }

  _search(name, dir) {
    if (!_.endsWith(name, '.js')) {
      name += '.js';
    }
    dir = _.defaultTo(dir, path.join(__dirname, '..'));

    debug('searching for "%s" in "%s"', name, dir);
    var files = fs.readdirSync(dir);
    if (_.includes(files, name)) {
      debug('file found in :', dir);
      return dir + '/' + name;
    }
    var ret;
    _.forEach(files, (file) => {
      file = dir + '/' + file;
      if (fs.statSync(file).isDirectory()) {
        ret = this._search(name, file);
      }
      return _.isNil(ret);
    });
    return ret;
  }

  _prepare(doc) {
    var section;
    doc.sections = [];
    doc.description = _.filter(_.split(doc.description, '\n'), function(line) {
      var match = line.match(PipoDoc.sectionRe);
      if (match) {
        section = _.upperCase(match[1]);
        section = { title: _.upperCase(match[1]), content: [] };
        doc.sections.push(section);
      }
      else if (!_.isNil(section)) {
        section.content.push(line);
      }
      else {
        return true;
      }
      return false;
    });
  }

  jsDoc(name, options) {
    try {
      name = this._search(name);
      debug('retriving doc for', name);
      var doc = jsdoc2md.getTemplateDataSync({ files: name })[0];
      this._prepare(doc);
      this._section(PipoDoc.title.DESCRIPTION, doc.description,
        PipoDoc.format.description);

      _.forEach(doc.sections, (sec) => {
        this._section(sec.title, sec.content, PipoDoc.format.description);
      });
      if (_.get(options, 'examples', false)) {
        _.forEach(doc.examples, (ex) => {
          this.fd.write('\n');
          this._section(PipoDoc.title.EXAMPLE, ex, PipoDoc.format.example);
        });
      }
      return true;
    }
    catch (e) {
      debug('Failed to generate doc: ', e);
      this.fd.write('Failed to generate doc: ' + e + '\n');
      return false;
    }
  }

  _section(title, content, format) {
    this.fd.write(title.bold.magenta);
    this.fd.write('\n');
    if (_.isString(content)) {
      content = _.split(content, '\n');
    }

    if (_.first(content) !== '\n') {
      this.fd.write('\n');
    }

    _.forEach(content, (line) => {
      line = _.reduce(format, formatter, line);
      this.fd.write('  ');
      this.fd.write(line);
      this.fd.write('\n');
    });
  }
}

_.assign(PipoDoc, {
  title: {
    DESCRIPTION: 'DESCRIPTION',
    EXAMPLE: 'EXAMPLE'

  },
  sectionRe: /^#+ (.*)$/,
  format: {
    description: [
      { re: /`([^`]*)`/g, cb: function(match, text) { return '`' + text.italic.cyan + '`'; } }
    ],
    example: [
      { re: /([0-9]+(:?\.[0-9]+)?|[{}\[\]:,])/g, cb: function(match) {
        /* must capture objects and numbers first since colors uses '[' and numbers */
        return ((_.size(match) === 1) && _.includes('{}\[\]:,', match)) ?
          match.dim : match.cyan;
      } },
      { re: /"((:?[^"]|\\.)+)"/g, cb: function(match, text) { return '"'.dim + text.magenta + '"'.dim; } },

      { re: /^\s*[=]+\s*$/, cb: function(match) { return match.yellow; } },
      { re: /^\s*\/\/.*$/, cb: function(match) { return match.dim; } }
    ]
  }
});

module.exports = PipoDoc;
