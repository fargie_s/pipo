'use strict'; /*jshint -W097 */

const
  _ = require('lodash'),
  q = require('q'),
  debug = require('debug')('q-ext');

class Lock {
  constructor(count) {
    this.count = count;
    this.locked = [];
  }

  wait(value) {
    if (this.count === 0) {
      var defer = q.defer();
      defer.resolve = defer.resolve.bind(defer, value);
      this.locked.push(defer);
      debug('locked(%d)', _.size(this.locked));
      return defer.promise;
    }
    this.count -= 1;
    return value;
  }

  post() {
    if (!_.isEmpty(this.locked)) {
      var defer = this.locked.shift();
      _.defer(defer.resolve);
    }
    else {
      this.count += 1;
    }
  }
}
q.Lock = Lock;

class Throttle {
  constructor(wait) {
    this.wait = wait;
    this.throttled = [];
    this.tick();
    this.timeout = null;
  }

  tick() {
    this.nextCall = this.wait + _.now();
  }

  _enqueue() {
    var defer = q.defer();
    if (_.isEmpty(this.throttled)) {
      this.timeout = setTimeout(this._timeout.bind(this),
        Math.max(this.nextCall - _.now(), 0));
    }
    this.throttled.push(defer);
    return defer.promise;
  }

  _timeout() {
    debug('running throttled');
    this.tick();
    /* run it in next loop */
    var defer = this.throttled.shift();
    _.defer(defer.resolve.bind(defer));
    if (!_.isEmpty(this.throttled)) {
      this.timeout = setTimeout(this._timeout.bind(this),
        Math.max(this.nextCall - _.now(), 0));
    }
  }

  promise() {
    if (_.isEmpty(this.throttled) && (this.nextCall <= _.now())) {
      debug('direct run');
      this.tick();
      return q();
    }
    return this._enqueue();
  }
}
q.Throttle = Throttle;

function streamReader(stream) {
  var defer = q.defer();
  var buff;

  function cleanup() {
    stream.removeListener('readable', onReadable);
    stream.removeListener('error', onError);
  }

  function onReadable() {
    var data = stream.read();
    debug('readable', data);
    if (_.isNil(data)) {
      cleanup();
      defer.resolve(buff);
    }
    else if (!_.isNil(buff)) {
      buff += data;
    }
    else {
      buff = data;
    }
  }
  function onError(err) {
    cleanup();
    defer.reject(err);
  }

  stream.on('readable', onReadable);
  stream.once('error', onError);

  if (stream.isPaused()) { stream.resume(); }

  return defer.promise;
}

/**
 * create a chain, calling function on provided items
 * @param  {function} func the function to call
 * @param  {} items array of items to call the function on
 * @return {promise}
 *
 * @example
 * q([ 1, 2, 3 ]).reduce(function(result, value) {
 *   return (result || 0) + value;
 * });
 * // is equivalent to
 * funcs.reduce(Q.when, Q(initialVal));
 * var prom = q();
 * [ 1, 2, 3 ].reduce(function(prom, value) {
 *   return prom.then(function(result) {
 *     return (result || 0) + value;
 *   });
 * }, q());
 */
q.reduce = function(items, func) {
  var prom = q();
  _.forEach(items, function(item, key) {
    prom = prom
    .then(_.bind(func, null, _, item, key, items));
  });
  return prom;
};

q.makePromise.prototype.reduce = function(func) {
  return this.then(_.partial(q.reduce, _, func));
};

/**
 * retry a promise returning function
 * @param  {function} func  the function to call
 * @param  {function} check the check function
 * @param  {integer}  max   maximum retry count (default: 3)
 * @param  {}         value func parameter[description]
 * @return {promise}
 *
 * @details check should return true when a retry should be attempted.
 */
q.retry = function(func, check, max, value) {
  if (_.isNil(max)) { max = 3; }
  return q(value)
  .then(func)
  .catch(function(err) {
    if ((max > 0) && check(err)) {
      return q.retry(func, check, max - 1, value);
    }
    throw err;
  });
};

q.makePromise.prototype.retry = function(func, check, max) {
  return this.then(_.partial(q.retry, func, check, max));
};

/**
 * Bind a native method using its name
 *
 * All arguments must be bound
 *
 * @param  {object} object the object to bind
 * @param  {string} methodName the method to bind
 * @param  {} args arguments to bind
 * @return {promise}
 */
function nmbind(object, methodName, ...args) {
  return _.ary(q.ninvoke.bind(null, object, methodName, ...args), 0);
}
q.nmbind = nmbind;

module.exports = q;
