'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-01-11T22:10:12+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  ora = require('ora'),
  _ = require('lodash'),
  EventEmitter = require('events'),
  dashdash = require('dashdash'),
  path = require('path'),
  fs = require('fs'),
  process = require('process'),
  timers = require('timers'),
  PipoDoc = require('./PipoDoc'),
  q = require('q'),
  debug = require('debug')('pipo:applet');

const pipo = require('../');

/**
 * return's an object or a prototype methods
 */
function methods(obj) {
  if (_.has(obj, 'prototype')) {
    obj = obj.prototype;
  }

  var ret = [];
  function pushFun(name) {
    if (_.isFunction(obj[name])) {
      ret.push(name);
    }
  }

  while (obj !== Object.prototype) {
    _.forEach(Object.getOwnPropertyNames(obj), pushFun);
    obj = Object.getPrototypeOf(obj);
  }
  return _.uniq(ret);
}

/**
 * create directory and sub-directories
 */
function qmkdir(dir) {
  return q.nfcall(fs.mkdir, dir)
  .catch((e) => {
    switch (e.code) {
      case 'EEXIST': return;
      case 'ENOENT': {
        var dirname = path.dirname(dir);
        if (dirname !== dir) {
          return qmkdir(dirname)
          .then(qmkdir.bind(null, dir));
        }
      }
    }
    throw e;
  })
  .catch((e) => {
    throw 'Failed to create directory: ' + e.message;
  });
}

/**
 * escape a string to be used as a shell argument
 */
function shellEscape(str) {
  return str.replace(/([ \t])/g, '\\$1');
}

class Applet {
  constructor(argv) {
    this.argv = argv;
  }

  runDefault() {
    var parser = dashdash.createParser({
      options: [
        { names: [ 'help', 'h' ], type: 'bool', help: 'Print this help and exit.' },
        { names: [ 'env' ], type: 'bool', help: 'Show applet env' },
        { names: [ 'install' ], type: 'bool', help: 'Install applets' },
        { names: [ 'install-dir' ], type: 'string', help: `Install directory (default: ${Applet.INSTALL_DIR})` },
        { names: [ 'clean' ], type: 'bool', help: 'Remove old applets' }
      ]
    });
    try {
        var opts = parser.parse(this.argv);
        if (opts.help) {
            var help = parser.help({ includeEnv: true }).trimRight();
            console.log('usage: pipo-applets --install [--install-dir <dir>]\n' +
              '       pipo-applets --env\n' +
              'details:\n' +
                        help);
        }
        else if (opts.env) {
          return this.runEnv(opts.install_dir);
        }
        else if (opts.install) {
          return this.install(
            _.defaultTo(opts.install_dir, Applet.INSTALL_DIR));
        }
    } catch (e) {
        console.error('pipo: error: %s', e.message);
        throw undefined;
    }
  }

  runEnv(dir) {
    dir = _.defaultTo(dir, Applet.INSTALL_DIR);
    var path = process.env["PATH"];
    if (_.includes(path, dir)) { return; }
    process.stdout.write('eval export PATH="$PATH:');
    process.stdout.write(shellEscape(dir));
    process.stdout.write('"\n');
  }

  checkSymlink(dir, link, applet) {
    link = path.join(dir, link);
    return q.nfcall(fs.readlink, link)
    .then(
      (tgt) => {
        if (tgt !== applet) {
          debug('invalid symlink "%s"', link);
          return q.nfcall(fs.unlink, link);
        }
        debug('valid symlink "%s"', link);
        return true;
      },
      () => {
        debug('not a symlink "%s"', link);
        return q.nfcall(fs.unlink, link);
      }
    );
  }

  createSymlinks(applet, dir, applets) {
    if (_.isEmpty(applets)) { return; }
    var name = _.first(applets);
    return q.nfcall(fs.symlink, applet, path.join(dir, name), 'file')
    .then(() => {
      debug('symlink created "%s"', name);
      return this.createSymlinks(applet, dir, _.slice(applets, 1));
    });
  }

  install(dir) {
    var spin = ora('Creating symlinks in ' + dir);
    var applet = path.resolve(_.get(this.argv, '[1]'));
    var applets = _.keys(pipo.Registry.pipes);
    return qmkdir(dir)
    .then(q.nfcall.bind(null, fs.readdir, dir))
    .then((olds) => {
      debug('checking symlinks');
      return q.all(_.map(olds, (old) => {
        if (_.includes(applets, old)) {
          return this.checkSymlink(dir, old, applet)
          .then((ret) => {
            if (ret === true) { _.pull(applets, old); }
          });
        }
        else {
          debug('not an applet "%s"', old);
          return q.nfcall(fs.unlink, path.join(dir, old));
        }
      }));
    })
    .then(() => {
      return this.createSymlinks(applet, dir, applets);
    })
    .then(
      spin.succeed.bind(spin, 'Symlinks created'),
      spin.fail.bind(spin)
    );
  }

  run() { // eslint-disable-line complexity
    var applet = path.basename(_.get(this.argv, '[1]', 'applet'));
    if (applet.startsWith('applet') || applet === "pipo-applet") {
      debug('running applet');
      return this.runDefault();
    }
    var PipeClass = pipo[applet];
    if (_.isNil(PipeClass)) {
      console.error('pipo: failed to find applet: %s', applet);
      throw undefined;
    }
    else if (_.includes(this.argv, '--help') || _.includes(this.argv, '-h')) {
      var doc = new PipoDoc(process.stdout);
      doc.jsDoc(applet);
      return;
    }
    else {
      debug('running %s', applet);
      return this.runPipe(PipeClass);
    }
  }

  runPipe(PipeClass) {
    return q(new pipo.StdIn())
    .then((pipeIn) => {
      var pipe = new PipeClass();
      var options = {};
      _.forEach(_.difference(methods(pipe), methods(EventEmitter.prototype)), (m) => {
        if (m.startsWith('set')) {
          var name = _.lowerFirst(m.slice(3));
          options[name] = { names: [ name ], type: 'string' };
        }
      });
      _.forEach(pipe, (value, key) => {
        if (key.startsWith('_') || _.hasIn(PipeClass.prototype, key)) { return; }
        options[key] = { names: [ key ], type: _.isBoolean(value) ? 'bool' : 'string' };
      });

      var opts = dashdash.createParser({ options: _.values(options) })
      .parse(this.argv);
      if (opts.help) {

        return;
      }

      opts = _.omit(opts, [ '_order', '_args' ]);
      pipe.configure(opts);

      var timer = timers.setInterval(function() {}, 500);
      pipeIn
      .next(pipe)
      .next(new pipo.StdOut())
      .once('end', () => {
        timers.clearInterval(timer);
      });

      _.invoke(pipeIn, 'start');
    })
    .done();
  }
}

Applet.INSTALL_DIR = '/usr/local/lib/pipo/bin';

module.exports = Applet;
