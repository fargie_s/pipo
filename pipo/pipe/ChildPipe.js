'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-09-19T22:08:46+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:child');

if (require.main === module) {

  const
    timers = require('timers'),
    pipo = require('../../pipo');

  debug('child created');
  /* keep process alive timer */
  let timer = timers.setInterval(_.noop, 10000);

  let pipe = new pipo.StdIn();
  let last = pipe
  .next(new pipo.SubPipe())
  .next(new pipo.StdOut())
  .once('end', () => {
    /* expire keepalive timer */
    timers.clearInterval(timer);
  });
  pipe.start();
}
else {

const
  child_process = require('child_process'),
  PipeElement = require('../PipeElement'),
  StdIn = require('../StdIn'),
  Item = require('../Item');

/**
 * @module ChildPipe
 * @description Run a SubPipe in a child process
 *
 * ### Configuration
 * | Name        | Type                    | Default | Description                         |
 * | :---------- | :---------------------- | :------ | :---------------------------------- |
 * | `pipe`      | string, array or object | null    | The sub-pipe to create              |
 * | [`oneShot`] | boolean                 | false   | Run the pipe once per incoming item |
 *
 * ### Details
 *
 * See *SubPipe* for details.
 */
class ChildPipe extends PipeElement {
  constructor(pipe) {
    super();
    this._opts = { noSeparateConfig: true, noSeparateError: true };
    this.oneShot = false;
    this.pipe = pipe;
  }

  setPipe(pipe) {
    if (this.pipe != pipe) {
      this.pipe = pipe;
      this._killChild();
    }
  }

  _killChild() {
    if (!_.isNil(this._child)) {
      let child = this._child;
      this._child = null;
      child.stdin.end();
    }
  }

  _createChild() {
    debug('creating child pipe');
    this.ref();
    let child = child_process.fork(__filename,
      { stdio: [ 'pipe', 'pipe', 'inherit', 'ipc' ] });
    let pipe = new StdIn(child.stdout);
    pipe.on('item', this.emitItem.bind(this));
    pipe.once('end', () => {
      this.unref();
    });
    child.once('error', (err) => {
      this.error(_.toString(err));
    });
    if (this.oneShot) {
      child.stdin.write('{"SubPipeConfig":{"oneShot":true}}');
    }
    child.stdin.write(JSON.stringify({ pipe: this.pipe }));
    this._child = child;
    return child;
  }

  onItem(item) {
    super.onItem(item);
    if (_.has(item, 'pipe')) {
      this.setPipe(Item.take(item, 'pipe'));
    }

    if (!_.isEmpty(item) && this.pipe) {
      let child = this._child;
      if (_.isNil(child)) {
        child = this._createChild();
      }
      child.stdin.write(JSON.stringify(item));
    }
    else {
      this.emitItem(item);
    }
  }

  unref() {
    super.unref();
    if (this._child && this._ref === 1) {
      this._child.stdin.end();
    }
  }
}

module.exports = ChildPipe;

}
