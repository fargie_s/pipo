'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2017-09-06T09:47:57+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  math = require('mathjs'),
  regression = require('regression'),
  _ = require('lodash'),
  debug = require('debug')('pipo:math'),
  Item = require('../Item'),
  PipeElement = require('../PipeElement');

math.import({
  count: function(val) { return _.size(MathEval.toJson(val)); },
  linReg: function(data) {
    data = MathEval.toJson(data);
    return regression.linear(data, { order: 10, precision: 10 }).equation;
  }
});

/**
 * @module MathEval
 * @description Evaluate a mathematical formula
 *
 * ### Configuration
 * | Name         | Type   | Default  | Description             |
 * | :----------- | :----- | :------- | :---------------------- |
 * | [`expr`]     | string | null     | Mathematical expression |
 * | [`property`] | string | 'result' | Output property         |
 *
 * ### Items
 * | Name   | Type   | Description             |
 * | :----- | :----- | :---------------------- |
 * | `expr` | string | Mathematical expression |
 *
 * ### Details
 * This element uses [Math.js](http://mathjs.org/docs/index.html).
 *
 * @example
 * {
 *   "pipe": "MathEval",
 *   "expr": "1 + a",
 *   "a": 41
 * }
 * ===
 * { "a": 41, "result": 42 }
 */
class MathEval extends PipeElement {
  constructor() {
    super();
    this.expr = null;
    this._code = null;
    this.property = "result";
  }

  setExpr(expr) {
    try {
      this._code = math.compile(expr);
      this.expr = expr;
    }
    catch (err) {
      debug(err);
      this.error(err);
    }
  }

  onItem(item) { // eslint-disable-line complexity
    super.onItem(item);

    var hasExpr = _.has(item, 'expr');
    if (hasExpr || (!_.isEmpty(item) && !_.isNil(this._code))) {
      try {
        let code = (hasExpr) ?
          math.compile(Item.take(item, 'expr')) : this._code;
        // FIXME: check why math.eval modifies item
        var ret = MathEval.toJson(code.eval(_.cloneDeep(item)));
        _.set(item, this.property, ret);
      }
      catch (err) {
        debug(err);
        if (hasExpr) {
          /* silently discarding if item doesn't have expr,
              not all packets are worth it */
          this.error(err);
        }
      }
    }
    this.emitItem(item);
  }

  static toJson(data) {
    if (_.hasIn(data, 'valueOf')) {
      data = data.valueOf();
    }
    if (_.isArray(data)) {
      data = _.map(data, MathEval.toJson);
    }
    return data;
  }
}

module.exports = MathEval;
