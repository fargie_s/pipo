'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-02-13T17:18:33+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  q = require('q'),
  _ = require('../utils/lodash-ext'),
  PipeElement = require('../PipeElement'),
  Item = require('../Item'),
  ChildPipe = require('../pipe/ChildPipe'),
  debug = require('debug')('pipo:ml');

class Generation
{
  constructor(item, varDesc, pipe, maxPipes) {
    this.var = varDesc;
    this.keys = _.keys(varDesc);
    this.pipe = pipe;
    this.pipes = [];
    this.maxPipes = maxPipes;
    this.count = 0;
    this.scale = 1;
    this._step;
    this.reset(item);
  }

  reset(item) {
    this.best = null;
    this.item = item;
    this.gen = null;
  }

  _score(input, vect, item) {
    if (Item.isError(item)) {
      this.errors = _.push(this.errors, item);
    }
    else if (!_.has(item, 'score')) {
      debug('bad item', item);
      this.errors = _.push(this.errors, { errorString: 'bad MLLocalMax item: ' +
        JSON.stringify(item, null, 0) });
    }
    else if (_.isNil(this.best) || (this.best.score < item.score)) {
      this.best = { vect: vect, score: item.score, item: input };
      return this.best;
    }
  }

  _createItem(vect) {
    let item = _.clone(this.item);
    _.forEach(this.keys, (key, idx) => { item[key] += vect[idx]; });
    return item;
  }

  _createPipe() {
    let pipe = new ChildPipe(this.pipe);
    pipe.oneShot = true;
    pipe.ref();
    this.pipes.push(pipe);
    return pipe;
  }

  _clearPipes() {
    _.forEach(this.pipes, function(pipe) { pipe.unref(); });
    this.pipes = [];
  }

  _runPipe(vect, pipe) {
    let defer = q.defer();
    let ref = this._createItem(vect);
    pipe.once('item', (item) => {
      _.defer(
        defer.resolve.bind(defer,
          this._score(ref, vect, item)
        )
      );
    });
    // duplicate ref and add a temporary 'generation' property
    pipe.onItem(_.assign({ generation: this.count }, ref));
    return defer.promise;
  }

  _prom(pipe) {
    let gen = this.gen.next();
    if (gen.done) { return null; }

    debug("Spawning step", ++this._step);
    pipe = pipe || this._createPipe();
    return this._runPipe(gen.value, pipe)
    .then(this._prom.bind(this, pipe));
  }

  _forward(strength, refVect) {
    if (_.isNil(this.best)) { return; }

    strength = strength || 2;
    refVect = refVect || this.best.vect;
    return this._runPipe(this._forwardVar(refVect, strength), _.first(this.pipes))
    .then((best) => {
      if (_.isNil(best)) { return null; }
      return this._forward(strength + 1, refVect);
    });
  }

  run() {
    debug('Running generation', ++this.count);
    this._step = 0;
    this.gen = this._genVar(_.clone(this.keys));
    let proms = [];

    for (let i = 0; i < this.maxPipes; ++i) {
      let prom = this._prom(this.pipes[i]);
      if (_.isNil(prom)) { break; }
      proms.push(prom);
    }
    return q.allSettled(proms)
    .then(() => {
      if (!_.isNil(this.best)) {
        debug('found vector', this.best.vect, this.best.score);
        return q()
        .then(this._forward.bind(this))
        .then(() => { debug('sized vector', this.best.vect, this.best.score); })
        .then(() => { return this.best; });
      }
      else {
        debug('no vector found');
        return null;
      }
    });
  }

  clear() {
    this._clearPipes();
  }

  _forwardVar(vect, strength) {
    vect = _.map(vect, _.partial(_.multiply, strength));
    for (var i = 0; i < vect.length; ++i) {
      let prop = this.keys[i];
      let desc = this.var[prop];
      if (_.has(desc, 'min') && ((this.item[prop] + vect[i]) < desc.min)) {
        vect[i] = desc.min - this.item[prop];
      }
      else if (_.has(desc, 'max') && ((this.item[prop] + vect[i]) > desc.max)) {
        vect[i] = desc.max - this.item[prop];
      }
    }
    return vect;
  }

  *_genVar(names) { // eslint-disable-line complexity
    if (_.isEmpty(names)) {
      yield [];
    }
    else {
      let prop = names.shift();
      let desc = this.var[prop];
      let ret = [ 0 ];
      if (!_.has(desc, 'min') || ((this.item[prop] - desc.step * this.scale) >= desc.min)) {
        ret.push([ -desc.step * this.scale ]);
      }
      if (!_.has(desc, 'max') || ((this.item[prop] + desc.step * this.scale) <= desc.max)) {
        ret.push([ desc.step * this.scale ]);
      }
      for (let vect of this._genVar(names)) {
        for (let val of ret) {
          yield _.concat(val, vect);
        }
      }
    }
  }

  upScale() {
    this.scale += 1;
    /* check if at least one vector will be generated */
    return _.some(this.var, (desc, prop) => {
      return (!_.has(desc, 'min') || ((this.item[prop] - desc.step * this.scale) >= desc.min)) ||
        (!_.has(desc, 'max') || ((this.item[prop] + desc.step * this.scale) <= desc.max));
    });
  }
}

/**
 * @module MLLocalMax
 * @description MLLocalMax find local maximum using generat
 *
 * ### Configuration
 * | Name        | Type                    | Default | Description            |
 * | :---------- | :---------------------- | :------ | :--------------------- |
 * | `pipe`      | string, array or object | null    | The sub-pipe to create |
 * | `var`       | object                  | null    | The ML parameters      |
 * | `maxPipes`  | integer                 | 10      | Maximum number of pipes to run in parallel |
 *
 * ### Variations
 * *var* descrcribes how properties may vary, for each varying property *var*
 * will have the following parameters:
 *
 * | Name        | Type   | Default | Description                |
 * | :---------- | :----- | :------ | :------------------------- |
 * | `step`      | number | null    | The property minimal step  |
 * | [`min`]     | number | null    | The property minimal value |
 * | [`max`]     | number | null    | The property maximal value |
 *
 * ### Items
 * This element should receive a single item that is the starting point of the
 * local-max research algorithm.
 *
 * ### Details
 * The *pipe* must return a single item containing a score property that must be
 * a number.
 *
 * @example
 * {
 *   "pipe": "MLLocalMax",
 *   "MLLocalMaxConfig": {
 *     "pipe": {
 *       "pipe": "MathEval",
 *       "MathEvalConfig": { "expr": "min(x, 10)", "property": "score" }
 *     },
 *     "var": {
 *       "x": { "step": 0.1 }
 *     }
 *   }
 *   "x": 0
 * }
 * ===
 * { "x": 10 }
 */
class MLLocalMax extends PipeElement {
  constructor() {
    super();
    this.pipe = null;
    this.var = null;
    this.maxPipes = 10;
    this.maxGen = 100;
  }

  _genLoop(gen, item, score) {
    if (gen.count >= this.maxGen) {
      debug('maximum generation count reached');
      return q(item);
    }
    return gen.run()
    .tap(() => {
      _.forEach(gen.errors, (e) => this.emitItem(e));
      gen.errors = [];
    })
    .then((ret) => {
      if (_.isNil(ret)) {
        debug('No vector found');
      }
      else if ((_.isNil(score) && !_.isNil(ret.score)) || (ret.score > score)) {
        ret.item.score = ret.score;
        debug('new best item: %s score: %d', JSON.stringify(ret.item, null, 0), ret.score);

        /* new score is better */
        gen.reset(ret.item);
        gen.scale = 1;
        return this._genLoop(gen, ret.item, ret.score);
      }
      else if (!_.isNil(ret.score)) {
        ret.item.score = ret.score;
        /* new score is not better, let's scan */
        gen.reset(ret.item);
        if (gen.upScale()) {
          // Looking for items a bit further
          // gen.scale will grow until a match is found
          debug('scanning (scale: %i) ...', gen.scale);
          return this._genLoop(gen, ret.item, ret.score);
        }
      }
      return item;
    });
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isNil(this.pipe) && !_.isNil(this.var) && !_.isEmpty(item)) {
      this.ref();
      let gen = new Generation(item, this.var, this.pipe, this.maxPipes);
      this._genLoop(gen, item)
      .then((item) => {
        this.emitItem(item);
      })
      .finally(() => { gen.clear(); this.unref(); });
    }
  }
}

module.exports = MLLocalMax;
