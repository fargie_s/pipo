/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-01-26T15:51:40+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  PipeElement = require('./PipeElement'),
  debug = require('debug')('pipo:merge');

/**
 * @module Merge
 * @description Merge incoming items
 *
 * ### Items
 * Incoming items are merged into a single item, the merge result is sent when
 * the pipe ends.
 *
 * @example
 * {
 *   "pipe": "Merge",
 * }
 * { "titi": { "foo": 42 } }
 * { "titi": { "bar": 24 } }
 * { "toto": 12 }
 * ===
 * {
 *   "titi": { "foo": 42, "bar": 24 },
 *   "toto": 12
 * }
 */
class Merge extends PipeElement {
  constructor() {
    super();
    this._item = null;
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isEmpty(item)) {
      this._item = _.merge(this._item, item);
    }
  }

  end(status) {
    if ((this._ref === 1) && !_.isNil(this._item)) {
      debug('merge result');
      this.emitItem(this._item);
    }
    super.end(status);
  }
}

module.exports = Merge;
