'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-09-14T07:58:43+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:tcp'),
  PipeElement = require('../PipeElement'),
  TCPIn = require('../input/TCPIn'),
  Item = require('../Item'),
  net = require('net');

/**
 * @module TCPOut
 * @description TCPIn output element
 *
 * ### Configuration
 * | Name         | Type    | Default | Description       |
 * | :----------- | :------ | :------ | :---------------- |
 * | `address`    | string  | null    | uri to connect to |
 *
 * @example
  * {
  *   "pipe": "TCPOut",
  *   "TCPOutConfig": { "address": "tcp://127.0.0.1:123456" },
  * }
  * { "name": "data to send" }
  * ===
  * { "name": "data to send" }
 */
class TCPOut extends PipeElement {
  constructor() {
    super();
    this.address = null;
    this._opts = { noSeparateConfig: true, noSeparateError: true };
  }

  setAddress(address) {
    if (address === this.address) {
      return;
    }
    this.ref();
    this.address = address;

    if (!_.isNil(this._conn)) {
      this._conn.end();
      this._conn = null;
    }

    var opts = TCPIn._parseAddr(address);
    if (_.isNil(address)) {
      this.unref();
    }
    else {
      var conn = net.createConnection(opts,
        () => debug('connected on', address));

      conn.once('error', (e) => {
        debug('connection error: ' + e);
        this.error('connection error:' + e);
        conn.end();
      });
      conn._endListener = () => {
        debug('remote connection closed');
        this.error('remote connection closed');
        conn.end();
      };
      conn.once('end', conn._endListener);
      conn.once('close', () => {
        if (this._conn === conn) {
          this._conn = null;
        }
        this.unref();
        conn.removeAllListeners();
      });
      this._conn = conn;
    }
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isNil(this._conn) && !_.isEmpty(item)) {
      this._conn.write(JSON.stringify(item, null, 0));
    }
    this.emitItem(item);
  }

  unref() {
    super.unref();
    if (!_.isNil(this._conn)) {
      if (this._ref === 1) {
        debug('closing connection');
        let conn = this._conn;
        this._conn = null;
        conn.removeListener('end', conn._endListener);
        conn.end();
      }
      else if (this._ref === 0) {
        debug('should not happen');
        this._conn.end();
        this._conn.removeAllListeners();
      }
    }
  }
}

module.exports = TCPOut;
