'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-08-08T10:09:12+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:sqlite'),
  Item = require('../Item'),
  SQLiteEval = require('../lang/SQLiteEval');

/**
 * @module SQLiteOut
 * @description SQLite output element
 *
 * ### Configuration
 * | Name         | Type    | Default | Description              |
 * | :----------- | :------ | :------ | :----------------------- |
 * | `database`   | string  | null    | Database file path       |
 * | `table`      | string  | null    | table to use             |
 * | `noBlobItem` | boolean | false   | disable blobItem support |
 *
 * When noBlobItem is false and the table has an *item* column with *BLOB*
 * affinity, then properties that are not already bound to a column are
 * serialized in the *item* column.
 *
 *
 * @example
 * # Inject elements:
 * {
 *   "pipe": "SQLiteEval|SQLiteOut",
 *   "SQLiteOutConfig": { "database": "test.sql", "table": "test" },
 *   "SQLiteEvalConfig": { "database": "test.sql" },
 *
 *   "sql": "CREATE TABLE test (date INTEGER PRIMARY KEY, name TEXT NOT NULL, item BLOB)",
 *   "date": 1234,
 *   "name": "toto",
 *   "additional": "value"
 * }
 *
 * # Now dump the table contents:
 * {
 *   "pipe": "SQLiteIn",
 *   "SQLiteInConfig": { "database": "test.sql", "table": "test" },
 *
 *   "where": "name = :name;",
 *   "name": "toto"
 * }
 * ===
 * { "name": "toto", "date": 1234, "additional": "value" }
 *
 */
class SQLiteOut extends SQLiteEval.Base {
  constructor() {
    super();
    this.table = null;
    this.noBlobItem = false;
  }

  _checkDB(table) {
    if (_.isNil(table)) {
      throw 'SQLite table not set';
    }
    return super._checkDB(false);
  }

  _getColInfo() {
    var colInfo = this._db.pragma(`table_info(${this.table})`);
    this._cols = _.map(colInfo, 'name');
    this._hasBlobItem = _.some(colInfo, { name: 'item', type: 'BLOB' });
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isEmpty(item) && !Item.isError(item)) {

      let table = _.defaultTo(Item.take(item, 'table'), this.table);

      try {
        if (this._checkDB(this.table)) {
          this._getColInfo();
        }

        if (this._hasBlobItem && !this.noBlobItem) {
          let cols = _.filter(this._cols, (col) => _.has(item, col) && col !== 'item');
          let stmt = this._db.prepare(`INSERT OR REPLACE INTO ${table} \
            (${cols.join(', ')}, item) \
            VALUES (${_.map(cols, (c) => ':' + c).join(', ')}, ?)`);
          stmt.run(item, JSON.stringify(_.omit(item, cols)));
        }
        else {
          let cols = _.filter(this._cols, (col) => _.has(item, col));
          let stmt = this._db.prepare(`INSERT OR REPLACE INTO ${table} \
            (${cols.join(', ')}) \
            VALUES (${_.map(cols, (c) => ':' + c).join(', ')})`);
          stmt.run(item);
        }
      }
      catch (e) {
        var message = _.get(e, 'message', e);
        debug('SQLite error:', message, 'code:', e.code);
        this.error(message);
      }
    }
    this.emitItem(item);
  }
}

module.exports = SQLiteOut;
