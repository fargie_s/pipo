'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-08-28T10:55:55+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:progress'),
  ora = require('ora'),
  Item = require('../Item'),
  PipeElement = require('../PipeElement');

/**
 * @module Progress
 * @description Render Handlebars/Mustache templates
 *
 * ### Configuration
 * | Name            | Type              | Default | Description         |
 * | :-------------- | :---------------- | :------ | :------------------ |
 * | `warnOnError`   | boolean           | true    | warn on error items |
 * | `persistOnEnd`  | boolean or string | false   | persist last message when all Progress pipes are stopped |
 *
 * ### Items
 * | Name        | Type   | Description                         |
 * | :---------- | :----- | :---------------------------------- |
 * | `text`      | string | display text and start progress     |
 * | [`warn`]    | string | display a persistent warning        |
 * | [`info`]    | string | display a persistent info message   |
 * | [`fail`]    | string | end progress with a failure message |
 * | [`succeed`] | string | end progress with a success message |
 *
 * ### Details
 *
 * The persistOnEnd property is shared for all Progress pipeElements, modifying
 * its value will modify all instances value.
 *
 * If persistOnEnd is a string its value will be the final message.
 *
 * @example
 * {
 *   "pipe": "Progress",
 *   "text": "Hello, world !",
 * }
 * ===
 * {
 * }
 */
class Progress extends PipeElement {
  constructor() {
    super();
    this.warnOnError = true;
    this._opts = { noSeparateError: true };
    Progress._ref += 1;
  }

  setPersistOnEnd(value) {
    Progress._persistOnEnd = value;
  }

  _ora() {
    return Progress._ora || (Progress._ora = ora());
  }

  onItem(item) { /* eslint-disable-line complexity */
    super.onItem(item);

    var spin = this._ora();
    var isSpinning = spin.isSpinning;
    if (_.has(item, 'text')) {
      spin.text = Item.take(item, 'text');
      isSpinning = true;
    }
    if (_.has(item, 'warn')) {
      this._ora().warn(Item.take(item, 'warn'));
    }
    if (_.has(item, 'info')) {
      this._ora().info(Item.take(item, 'info'));
    }
    if (_.has(item, 'succeed')) {
      this._ora().succeed(Item.take(item, 'succeed'));
      isSpinning = false;
    }
    if (_.has(item, 'fail')) {
      this._ora().fail(Item.take(item, 'fail'));
      isSpinning = false;
    }
    if (this.warnOnError && Item.isError(item)) {
      this._ora().warn(_.get(item, 'errorString'));
    }
    if (isSpinning && !spin.isSpinning) {
      debug('starting ora');
      spin.start(spin.text);
    }

    this.emitItem(item);
  }

  unref() {
    if ((this._ref === 1) && (--Progress._ref <= 0)) {
      if (_.isString(Progress._persistOnEnd)) {
        this._ora().succeed(Progress._persistOnEnd);
      }
      else if (Progress._persistOnEnd) {
        this._ora().stopAndPersist();
      }
      this._ora().stop();
      Progress._persistOnEnd = PERSIST_DEFAULT;
    }
    super.unref();
  }
}

const PERSIST_DEFAULT = false;

Progress._ref = 0;
Progress._persistOnEnd = PERSIST_DEFAULT;

module.exports = Progress;
