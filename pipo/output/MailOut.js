'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-01-04T10:11:54+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('../utils/lodash-ext'),
  debug = require('debug')('pipo:mail'),
  Item = require('../Item'),
  nodemailer = require('nodemailer'),
  PipeElement = require('../PipeElement');

/**
 * @module MailOut
 * @description Mail sender element
 *
 * ### Configuration
 * | Name         | Type             | Default                       | Description                 |
 * | :----------- | :--------------- | :---------------------------  | :-------------------------- |
 * | `server`     | string or object | null                          | SMTP server configuration   |
 * | [`from`]     | string           | "Pipo <piper.pipo@gmail.com>" | from field configuration    |
 * | [`to`]       | string           | null                          | to field configuration      |
 * | [`subject`]  | string           | "pipo the piper"              | subject field configuration |
 * | [`itemName`] | string           | "item.json"                   | add item as an attachment   |
 *
 * ### Items
 * | Name            | Type   | Description                 |
 * | :-------------- | :----- | :-------------------------- |
 * | `text`          | string | text body                   |
 * | [`html`]        | string | HTML body                   |
 * | [`attachments`] | object | Attached files              |
 * | [`from`]        | string | from field configuration    |
 * | [`to`]          | string | to field configuration      |
 * | [`subject`]     | string | subject field configuration |
 *
 * ### Details
 * When *server* property is an URL, the login and password parts must be URL
 * encoded.
 *
 *
 * @example
 * {
 *   "pipe": "MailOut",
 *   "MailOutConfig": {
 *     "server": {
 *       "host": "smtp.gmail.com", "secure": true,
 *       "auth": { "user": "piper.pipo", "pass": "..." }
 *     }
 *   }
 * }
 * { "to": "piper.pipo@gmail.com", "text": "this is a test" }
 *
 * @example
 * {
 *   "pipe": "MailOut",
 *   "MailOutConfig": {
 *     "server": "smtps://user:pass@smtp.gmail.com"
 *   }
 * }
 * { "to": "piper.pipo@gmail.com", "text": "this is a test" }
 */
class MailOut extends PipeElement {
  constructor() {
    super();
    this.server = null;
    this.from = 'Pipo <piper.pipo@gmail.com>';
    this.to = null;
    this.subject = 'pipo the piper';
    this.itemName = 'item.json';
  }

  setServer(server) {
    if (!_.isNil(this._transport)) {
      delete this._transport;
    }
    this.server = server;
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isNil(this.server)) {
      this._sendMail(item);
    }

    this.emitItem(item);
  }

  _sendMail(item) {
    var mail = {};
    _.forEach([ 'from', 'to', 'subject' ], (prop) => {
      var value = _.defaultTo(Item.take(item, prop), this[prop]);
      if (!_.isNil(value)) {
        mail[prop] = value;
      }
    });
    _.forEach([ 'text', 'html', 'attachments' ], function(prop) {
      var value = Item.take(item, prop);
      if (!_.isNil(value)) {
        mail[prop] = value;
      }
    });

    if (!_.has(mail, "text") || !_.has(mail, "to")) { return; }

    if (!_.isEmpty(item) && this.itemName) {
      mail.attachments = _.push(mail.attachments, {
        filename: this.itemName, content: JSON.stringify(item),
        contentType: 'application/json'
      });
    }

    if (_.isNil(this._transport)) {
      this._transport = nodemailer.createTransport(this.server);
    }
    this.ref();
    this._transport.sendMail(mail, (err) => {
      if (err) {
        this.error(`Failed to send mail: ${err}`);
      }
      else {
        debug('mail sent');
      }
      this.unref();
    });
  }
}

module.exports = MailOut;
