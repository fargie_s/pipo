'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-08-06T09:48:36+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:sqlite'),
  PipeElement = require('../PipeElement'),
  Item = require('../Item'),
  SQLiteEval = require('../lang/SQLiteEval');

/**
 * @module SQLiteIn
 * @description SQLite input element
 *
 * ### Configuration
 * | Name         | Type    | Default | Description              |
 * | :----------- | :------ | :------ | :----------------------- |
 * | `database`   | string  | null    | Database file path       |
 * | `table`      | string  | null    | table to use             |
 * | `noBlobItem` | boolean | false   | disable blobItem support |
 *
 * ### Items
 * | Name      | Type   | Description      |
 * | :-------- | :----- | :--------------- |
 * | `where`   | string | SQL where clause |
 * | [`table`] | string | table to use     |
 *
 * Extra attributes can be bound in the statement (see examples).
 *
 * When noBlobItem is false and the table has an *item* column its content
 * is deserialized and assigned in the associated row.
 *
 * If blobItem deserialization fails, the row is not modified.
 *
 * @example
  * {
  *   "pipe": "SQLiteIn",
  *   "SQLiteInConfig": { "database": "test.sql", "table": "test" }
  * }
  * {
  *   "where": "name = :name;",
  *   "name": "toto"
  * }
  * ===
  * { "name": "toto", "date": 1234 }
  * { "name": "toto", "date": 1235 }
 */
class SQLiteIn extends SQLiteEval.Base {
  constructor() {
    super();
    this.table = null;
    this.noBlobItem = false;
  }

  _checkDB(table) {
    if (_.isNil(table)) {
      throw 'SQLite table not set';
    }
    super._checkDB(true);
  }

  _parseBlobItem(row) {
    if (!this.noBlobItem && _.has(row, 'item')) {
      try {
        var item = JSON.parse(row.item);
        if (!_.isObject(item)) {
          throw 'not an object';
        }
        _.unset(row, 'item');
        row = _.assign(row, item);
      }
      catch (e) {
        debug('Failed to parse blobItem:', e);
      }
    }
    return row;
  }

  onItem(item) {
    super.onItem(item);

    if (_.has(item, 'where')) {
      let where = Item.take(item, 'where');
      let table = _.defaultTo(Item.take(item, 'table'), this.table);
      let boundParams = this.boundParams(where);

      try {
        this._checkDB(table);
        let stmt = this._db.prepare(`SELECT * FROM ${table} WHERE ${where}`);

        for (var row of stmt.iterate(item)) {
          row = this._parseBlobItem(row);
          this.emitItem(row);
        }
      }
      catch (e) {
        var message = _.get(e, 'message', e);
        debug('SQLite error:', message, 'code:', e.code);
        this.error(message);
      }
      _.forEach(boundParams, function(param) {
        _.unset(item, param);
      });
    }

    if (!_.isEmpty(item)) {
      this.emitItem(item);
    }
  }
}

module.exports = SQLiteIn;
