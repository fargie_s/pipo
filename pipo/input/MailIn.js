'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-01-05T18:49:43+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  q = require('q'),
  simpleParser = require('mailparser').simpleParser,
  debug = require('debug')('pipo:mail'),
  Imap = require('imap'),
  { URL } = require('url'),
  querystring = require('querystring'),
  PipeElement = require('../PipeElement');


class Mail {
  constructor(mailIn, msg, opts) {
    this.msg = msg;
    this.mailIn = mailIn;
    this.opts = opts;
  }

  _parseItem(buffer, isError) {
    try {
      var item = JSON.parse(buffer);
      if (_.isObject(item) && !_.isArray(item)) {
        this.mailIn.emitItem(item);
        return 1;
      }
    }
    catch (e) {
      debug('failed to parse attachment:', e.message);
      if (isError) {
        this.mailIn.error('failed to parse json attachment');
      }
    }
    return 0;
  }

  _findItems(msg) {
    var ret = 0;
    _.forEach(msg.attachments, (attach) => {
      if (!_.includes([ 'application/json', 'text/plain' ], attach.contentType)) {
        debug('Ignoring unknown attachment:', attach.contentType);
        return;
      }
      ret += this._parseItem(attach.content, attach.contentType === 'application/json');
    });
    ret += this._parseItem(msg.text, false);
    return ret;
  }

  _mailAddr(addrs) {
    var ret = _.map(addrs, function(addr) {
      return _.isEmpty(_.get(addr, 'name')) ? _.get(addr, 'address') : addr;
    });
    if (_.isEmpty(ret)) { return null; }
    return (_.size(ret) === 1) ? ret[0] : ret;
  }

  _mailItem(msg) {
    this.mailIn.emitItem(_.omitBy({
      from: this._mailAddr(_.get(msg, 'from.value')),
      to: this._mailAddr(_.get(msg, 'to.value')),
      cc: this._mailAddr(_.get(msg, 'cc.value')),
      messageId: _.get(msg, 'messageId'),
      subject: _.get(msg, 'subject'),
      date: _.toString(_.get(msg, 'date')),
      text: _.get(msg, 'text'),
      attachments: _.map(_.get(msg, 'attachments'), (attach) => {
        return {
          content: attach.content.toString('base64'),
          contentType: attach.contentType,
          filename: attach.filename
        };
      })
    }, _.isNil));
    return 1;
  }

  promise() {
    var refs = 1;
    var defer = q.defer();
    if (this.opts.markSeen) {
      var attrsProm = q.defer();
      this.msg.once('attributes', (value) => { attrsProm.resolve(value); });
    }
    this.msg.once('body', (stream) => {
      refs += 1;
      q(simpleParser(stream)
      .then((data) => {
        var count = (this.opts.raw) ? this._mailItem(data) : this._findItems(data);
        if ((count > 0) && this.opts.markSeen) {
          return attrsProm.promise
          .then((attrs) => {
            return q.ninvoke(this.mailIn._server, 'addFlags',
              attrs.uid, [ '\\Seen' ]);
          });
        }
      }))
      .finally(() => {
        if (--refs === 0) { defer.resolve(); }
      })
      .catch((e) => { debug('Error', e); });
    });
    this.msg.once('end', () => {
      if (--refs === 0) { defer.resolve(); }
      debug('mail retrieved', refs);
    });
    return defer.promise;
  }
}


/**
 * @module MailIn
 * @description Mail retriever element
 *
 * ### Configuration
 * | Name         | Type             | Default  | Description                          |
 * | :----------- | :--------------- | :------  | :----------------------------------- |
 * | `server`     | string or object | null     | IMAP server configuration            |
 * | [`box`]      | string           | 'INBOX'  | Mailbox to fetch                     |
 * | [`fetch`]    | boolean          | true     | Fetch unseen messages                |
 * | [`watch`]    | boolean          | false    | Wait and watch for incoming messages |
 * | [`raw`]      | boolean          | false    | Return complete mail as an item      |
 * | [`markSeen`] | boolean          | true     | Flag pipo messages as Seen           |
 *
 */
class MailIn extends PipeElement {
  constructor() {
    super();
    this.server = null;
    this.box = 'INBOX';
    this.fetch = true;
    this.watch = false;
    this.raw = false;
    this.markSeen = true;
    this._q = q(); /* use a queue to make calls synchronous */
  }

  _closeBox() {
    if (_.isNil(this._box)) { return q(); }
    return q.ninvoke(this._server, 'closeBox')
    .finally(() => {
      this._box = null;
      debug('mailbox closed');
    });
  }

  _closeServer() {
    if (_.isNil(this._server)) { return q(); }
    return this._closeBox()
    .catch(this.error.bind(this))
    .then(() => {
      this._server.removeAllListeners();
      this._server.end();
    })
    .finally(() => {
      this._server = null;
      _.defer(this.unref.bind(this));
      debug('connection closed');
    });
  }

  _openBox() {
    if (_.isNil(this._server)) { return q(); }
    return this._closeBox()
    .then(_.ary(q.ninvoke.bind(null, this._server, 'openBox', this.box, !this.markSeen), 0))
    .then((box) => {
      this._box = box;
      debug('mailbox opened "%s"', this.box);
    });
  }

  _parseUrl(server) {
    if (_.isString(server)) {
      var url = new URL(server);
      if (!_.includes([ 'imap:', 'imaps:' ], url.protocol)) {
        throw 'Unknown protocol:' + url.protocol;
      }
      var ret = _.omitBy({
        host: url.host, port: url.port, tls: (url.protocol === 'imaps:'),
        user: url.username, password: url.password
      }, _.isNil);
      if (_.isEmpty(ret.port)) { ret.port = (ret.tls ? 993 : 143); }
      _.forEach([ 'user', 'password'], function(key) {
        if (!_.isNil(ret[key])) { ret[key] = querystring.unescape(ret[key]); }
      });
      return ret;
    }
    return server;
  }

  _openServer(server) {
    this.ref();
    return this._closeServer()
    .then(this._parseUrl.bind(this, server))
    .then((server) => {
      // server.debug = debug;
      var def = q.defer();
      this._server = new MailIn.Imap(server);
      this._server.once('ready', def.resolve.bind(def));
      this._server.once('error', def.reject.bind(def));
      this._server.connect();
      return def.promise;
    })
    .then(() => {
      this._server.removeAllListeners('error');
      debug('connected');
      return this._openBox();
    })
    .then(() => {
      if (!this.fetch) { return; }
      return this._fetchUnseen();
    })
    .then(() => {
      if (this.watch) {
        return this._watch();
      }
      return this._closeServer();
    })
    .catch((e) => {
      this.error(`Failed to open mailbox ${e}`);
      if (this._server) {
        this._server.removeAllListeners('ready');
        return this._closeServer();
      }
      _.defer(this.unref.bind(this));
    });
  }

  _fetchUnseen() {
    debug('fetching mails');
    return q.ninvoke(this._server, 'search', [ 'UNSEEN' ])
    .then(this._fetch.bind(this))
    .tap(() => { debug('done fetching'); });
  }

  _fetch(results) {
    if (_.isEmpty(results)) { return q(); }

    var opts = { markSeen: this.markSeen, raw: this.raw };
    var refs = 1;
    var defer = q.defer();
    var f = this._server.fetch(results, { bodies: '' });
    f.on('message', (msg) => {
      refs += 1;
      (new Mail(this, msg, opts)).promise()
      .finally(() => {
        if (--refs === 0) { defer.resolve(); }
      });
    });
    f.once('error', (err) => {
      this.error(`Failed to fetch: ${err}`);
      debug('fetch error:', err);
      defer.reject();
    });
    f.once('end', () => {
      if (--refs === 0) { defer.resolve(); }
    });
    return defer.promise;
  }

  _watch() {
    this._server.on('mail', (count) => {
      var last = this._box.messages.total + 1;
      this._fetch((last - count) + ':' + last);
    });
  }

  setServer(server) {
    this.server = server;
    if (this._started) {
      this._q = this._q.then(_.noop, _.noop)
      .then(this._openServer.bind(this, server));
    }
  }

  start() {
    super.start();
    if (_.isNil(this._server) && !_.isNil(this.server)) {
      this._q = this._q.then(_.noop, _.noop)
      .then(this._openServer.bind(this, this.server));
    }
  }
}

MailIn.Imap = Imap;

module.exports = MailIn;
