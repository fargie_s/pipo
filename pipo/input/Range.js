'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-02-13T13:34:55+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  PipeElement = require('../PipeElement'),
  debug = require('debug')('pipo:range');

/**
 * @module Range
 * @description Range generator
 *
 * ### Configuration
 * | Name        | Type   | Default  | Description          |
 * | :---------- | :----- | :------- | :------------------- |
 * | [`start`]    | number | 0        | Range starting index |
 * | `end`      | number |         | Range end index index |
 * | [`step`]  | number | 1 | Range step |
 * | [`property`] | string | "value" | the index property |
 *
 * ### Details
 * This item will generate a range from *start* up to, but not including, *end*.
 *
 * @example
 * {
 *   "pipe": "Range",
 *   "RangeConfig": {
 *     "end": 10,
 *     "step": 2
 *   }
 * }
 * ===
 * { "value": 0 }
 * { "value": 2 }
 * { "value": 4 }
 * { "value": 6 }
 * { "value": 8 }
 */
class Range extends PipeElement {
  constructor() {
    super();
    this._start = 0; /* can't call it start (already a method) */
    this._end = null; /* can't call it end (already a method) */
    this.step = 1;
    this.property = 'value';
    this._done = false;
  }

  setStart(start) {
    this._start = start;
  }
  setEnd(end) {
    this._done = false;
    this._end = end;
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isNil(this._end) && !this._done) {
      debug('generating range [%s, %s[', this._start, this._end);
      this._done = true;
      let step = Math.abs(this.step);
      if (this._start < this._end) {
        for (let i = this._start; i < this._end; i += step) {
          this.emitItem({ [this.property]: i });
        }
      }
      else {
        for (let i = this._start; i > this._end; i -= step) {
          this.emitItem({ [this.property]: i });
        }
      }
    }
  }

  start() {
    super.start();

    this.onItem({});
  }
}

module.exports = Range;
