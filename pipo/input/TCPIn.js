'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-09-13T23:15:21+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:tcp'),
  DataIn = require('../DataIn'),
  PipeElement = require('../PipeElement'),
  Item = require('../Item'),
  net = require('net');

/**
 * @module TCPIn
 * @description TCPIn input element
 *
 * ### Configuration
 * | Name         | Type              | Default | Description              |
 * | :----------- | :---------------- | :------ | :----------------------- |
 * | `address`    | string or number  | null    | port or uri to listen on |
 * | `autoClose`  | boolean           | false   | automaticaly close when last opened connection finishes |
 *
 * ### Details
 *
 * This pipe listens on a TCP port for incoming items.
 *
 * The address can be URI encoded, but it then must start with "tcp://"
 *
 * @example
  * {
  *   "pipe": "TCPIn",
  *   "TCPInConfig": { "address": "tcp://*:123456" },
  * }
  * ===
  * { "name": "remote data" }
 */
class TCPIn extends PipeElement {
  constructor() {
    super();
    this.address = null;
    this.autoClose = false;
  }

  static _parseAddr(address) {
    if (_.isNumber(address)) {
      return { port: address };
    }
    else if (_.isString(address)) {
      if (_.startsWith(address, 'tcp://')) {
        address = address.substr(6);
      }
      address = address.split(':');
      address[1] = parseInt(address[1]);
      if (_.size(address) !== 2 || !_.isNumber(address[1])) {
        return null;
      }
      else {
        return { host: address[0], port: address[1] };
      }
    }
    return null;
  }

  _removeConn(c) {
    if (_.isNil(c)) {
      return;
    }
    _.pull(this._conn, c);
    c.end();
  }

  _onConnection(c) {
    c.dataIn = new DataIn();
    c.dataIn.next(this);
    c.dataIn.ref();
    this._conn.push(c);
    this._hadConn = true;
    c.once('error', (e) => {
      debug('connection error');
      this.error(e);
      this._removeConn(c);
    });
    c.once('end', () => {
      debug('client connection closed');
      this._removeConn(c);
    });
    c.once('close', () => {
      debug('client connection terminated')
      c.removeAllListeners();
      if (!_.isNil(c.dataIn)) {
        c.dataIn.end(0);
        c.dataIn = null;
      }
    });
    c.on('data', function(buf) {
      if (!_.isNil(c.dataIn)) {
        c.dataIn.add(buf);
      }
    })
  }

  setAddress(address) {
    if (address === this.address) {
      return;
    }
    this.ref();
    this.address = address;

    if (!_.isNil(this._server)) {
      this._server.close();
      _.forEach(this._conn, function(c) { c.end(); });
    }

    this._server = null;
    this._conn = [];
    var opts = TCPIn._parseAddr(address);
    if (_.isNil(address)) {
      this.unref();
    }
    else if (_.isNil(opts)) {
      this.error('failed to parse address: ' + address);
    }
    else {
      var server = net.createServer(this._onConnection.bind(this));
      server.once('listening', () => debug('listening on', this.address));
      server.once('error', (e) => {
        debug('server error');
        this.error(e);
        server.close();
      });
      server.once('close', () => {
        debug('server closed');
        server.removeAllListeners();
        this.unref();
      });
      server.listen(opts);
      this._server = server;
    }
  }

  onItem(item) {
    super.onItem(item);
    this.emitItem(item);
  }

  unref() {
    super.unref();
    if ((this._ref === 1) && !_.isNil(this._server)) {
      if (this.autoClose && this._hadConn) {
        this._server.close();
      }
    }
  }
}

module.exports = TCPIn;
