'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-04-10T12:17:11+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  sa = require('superagent'),
  debug = require('debug')('pipo:http'),
  PipeElement = require('../PipeElement'),
  Item = require('../Item');

/**
 * @module HTTPPost
 * @description Send an HTTP POST request
 *
 * ### Configuration
 * | Name       | Type    | Default | Description      |
 * | :--------- | :-------| :------ | :--------------- |
 * | `rawReply` | boolean | false   | return raw reply |
 *
 * ### Items
 * | Name            | Type   | Description     |
 * | :-------------- | :----- | :-------------- |
 * | `url`           | string | URL to request  |
 * | [`headers`]     | object | headers to set  |
 * | [`data`]        | any    | payload to send |
 *
 * @example
 * {
 *   "pipe": "HTTPPost",
 *   "url": "https://www.euronext.com/en/popup/data/download",
 *   "headers": { "Content-Type": "application/x-www-form-urlencoded" },
 *   "data": {
 *     "format": 2, "layout": 2,
 *     "decimal_separator": 1, "date_format": 1,
 *     "op": "Go", "form_build_id": "form-1234",
 *     "form_id": "nyx_download_form"
 *   }
 * }
 * ===
 * {
 *   "data": "..."
 * }
 */
class HTTPPost extends PipeElement {
  constructor() {
    super();
    this.rawReply = false;
  }

  onItem(item) {
    super.onItem(item);

    if (_.hasIn(item, 'url')) {
      var req = sa.post(Item.take(item, 'url'));
      _.forEach(item.headers, function(value, key) {
        req.set(key, value);
      });
      _.unset(item, 'headers');
      if (_.hasIn(item, 'query')) {
        req.query(Item.take(item, 'query'));
      }

      if (_.hasIn(item, 'data')) {
        req.send(Item.take(item, 'data'));
      }
      this.ref();
      req.then(
        (reply) => {
          this.emitItem(_.omitBy({
            body: _.isEmpty(reply.body) ? reply.text : reply.body,
            headers: reply.headers
          }, _.isNil));
          this.unref();
        },
        (err) => {
          debug(err);
          this.error(_.get(err, 'message', err));
          this.unref();
        }
      );
    }
    this.emitItem(item);
  }
}

module.exports = HTTPPost;
