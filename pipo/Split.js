'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-09-27T20:58:10+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash');

const
  PipeElement = require('./PipeElement'),
  Item = require('./Item');

/**
 * @module Split
 * @description Split items or properties
 *
 * ### Configuration
 *
 * | Name         | Type   | Default | Description             |
 * | :----------- | :----- | :------ | :---------------------- |
 * | [`property`] | string | null    | The properties to split |
 *
 * ### Items
 * Whenever *property* is set this property will be split from incoming items.
 *
 * If *property* is null, complete items are extracted from *items* property and
 * sent as new items.
 *
 * @example
 * // Splitting items
 * {
 *   "pipe": "Split",
 *   "items": [ { "test": 42 }, { "titi" : 44 } ],
 *   "toto": 44
 * }
 * ===
 * { "test": 42 }
 * { "titi": 44 }
 * { "toto": 44 }
 *
 * @example
 * // Splitting properties
 * {
 *   "pipe": "Split",
 *   "SplitConfig": { "property": "test" },
 *   "test": [ 42, 45 ],
 *   "titi": 43
 * }
 * ===
 * { "test": 42, "titi": 43 }
 * { "test": 45, "titi": 43 }
 */
class Split extends PipeElement {
  constructor() {
    super();
    this.property = null;
  }

  onItem(item) {
    super.onItem(item);

    if (_.isNil(this.property)) {
      _.forEach(Item.take(item, 'items'), (value) => {
        if (_.isObject(value)) {
          this.emitItem(value);
        }
      });
      this.emitItem(item);
    }
    else if (_.has(item, this.property)) {
      _.forEach(Item.take(item, this.property), (value) => {
        this.emitItem(_.set(_.cloneDeep(item), this.property, value));
      });
    }
    else {
      this.emitItem(item);
    }
  }
}

module.exports = Split;
