/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-04-23T10:43:39+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  PipeElement = require('./PipeElement');

/**
 * @module Keep
 * @description Keep only specified properties
 *
 * ### Configuration
 * | Name         | Type         | Default | Description                            |
 * | :----------- | :----------- | :------ | :------------------------------------- |
 * | `properties` | string array | null    | Properties to keep in incoming objects |
 *
 * @example
 * {
 *   "pipe": "Keep",
 *   "KeepConfig": { "properties": [ "test", "toto" ] }
 * }
 * {
 *   "test": 42,
 *   "titi": 44
 * }
 * ===
 * { "test" : 42 }
 */
class Keep extends PipeElement {
  constructor() {
    super();
    this.properties = null;
  }

  onItem(item) {
    super.onItem(item);

    if (!_.isNil(this.properties)) {
      _.forIn(item, (value, key, item) => {
        if (!_.includes(this.properties, key)) {
          _.unset(item, key);
        }
      });
    }
    this.emitItem(item);
  }
}

module.exports = Keep;
