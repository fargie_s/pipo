'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-01-26T18:33:07+01:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  vm = require('vm'),
  Item = require('../Item'),
  debug = require('debug')('pipo:js'),
  _ = require('lodash'),
  PipeElement = require('../PipeElement');

/**
 * @module JSEval
 * @description Evaluate a Javascript expression
 *
 * ### Configuration
 * | Name         | Type   | Default  | Description           |
 * | :----------- | :----- | :------- | :-------------------- |
 * | [`expr`]     | string | null     | Javascript expression |
 * | [`property`] | string | 'result' | Output property       |
 *
 * ### Items
 * | Name   | Type   | Description           |
 * | :----- | :----- | :-------------------- |
 * | `expr` | string | Javascript expression |
 *
 * @example
 * {
 *   "pipe": "JSEval",
 *   "expr": "item.a += 1",
 *   "a": 41
 * }
 * ===
 * { "a": 42 }
 */
class JSEval extends PipeElement {
  constructor() {
    super();
    this.expr = null;
    this._script = null;
    this.property = "result";
  }

  setExpr(expr) {
    if (expr !== this.expr) {
      this._script = null;
      this.expr = expr;
    }
  }

  _getScript() {
    this._script = this._script ||
      new vm.Script(this.expr, { filename: 'expr' });
    return this._script;
  }

  onItem(item) { // eslint-disable-line complexity
    super.onItem(item);

    var hasExpr = _.has(item, 'expr');
    if (hasExpr || (!_.isEmpty(item) && !_.isNil(this.expr))) {
      try {
        debug('running expr');
        var script = (hasExpr) ?
          new vm.Script(Item.take(item, 'expr'), { filename: 'expr' }) :
          this._getScript();
        var ret = script.runInNewContext({ item: item });
        if (!_.isUndefined(ret)) {
          _.set(item, this.property, ret);
        }
      }
      catch (err) {
        debug(err);
        if (hasExpr) {
          /* silently discarding if item doesn't have expr,
              not all packets are worth it */
          this.error(err);
        }
      }
    }
    this.emitItem(item);
  }
}

module.exports = JSEval;
