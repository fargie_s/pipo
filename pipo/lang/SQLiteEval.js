'use strict';
/*
** Copyright (C) 2018 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2018-08-04T13:59:06+02:00
**     Author: Sylvain Fargier <fargie_s> <fargier.sylvain@free.fr>
**
*/

const
  _ = require('lodash'),
  debug = require('debug')('pipo:sqlite'),
  Item = require('../Item'),
  PipeElement = require('../PipeElement'),
  Database = require('better-sqlite3');

class SQLiteBase extends PipeElement {
  constructor() {
    super();
    this.database = null;
  }

  setDatabase(database) {
    if (this.database !== database) {
      this.database = database;
      if (!_.isNil(this._db)) {
        this._db.close();
        this._db = null;
      }
    }
  }

  boundParams(stmt) {
    stmt = _.get(stmt, 'source', stmt);
    if (!_.isString(stmt)) {
      return [];
    }
    let result = [];
    let match;
    while ((match = SQLiteBase._paramRe.exec(stmt)) !== null) {
      result.push(match[1]);
    }
    return result;
  }

  _checkDB(readonly) {
    if (_.isNil(this.database)) {
      throw 'SQLite database not set';
    }
    else if (_.isEmpty(this.database)) {
      /* disable in-memory databases */
      throw 'A database filename cannot be an empty string';
    }
    else if (_.isNil(this._db)) {
      debug('opening db:', this.database);
      this._db = new Database(this.database, { readonly });
      // this._db.pragma('busy_timeout = 10000');
      if (!readonly) {
        this._db.pragma('journal_mode = WAL');
      }
      return true;
    }
    return false;
  }

  unref() {
    super.unref();
    if ((this._ref === 0) && !_.isNil(this._db)) {
      this._db.close();
    }
  }
}

SQLiteBase._paramRe = /[$:@]([a-zA-Z0-9]+)/g;

/**
 * @module SQLiteEval
 * @description Create an SQLite database
 *
 * ### Configuration
 * | Name       | Type    | Default | Description              |
 * | :--------- | :------ | :------ | :----------------------- |
 * | `database` | string  | null    | Database file path       |
 * | `readonly` | boolean | false   | Set database as readonly |
 *
 * ### Items
 * | Name         | Type                   | Description             |
 * | :----------- | :--------------------- | :---------------------- |
 * | `sql`        | string or string array | SQL statement(s) to run |
 *
 * Extra attributes can be bound in the statement (see examples).
 *
 * @example
 * {
 *   "pipe": "SQLiteEval",
 *   "SQLiteEvalConfig": { "database": "test.sql" },
 * }
 * {
 *   "sql": "SELECT * from test WHERE name = :name;",
 *   "name": "toto"
 * }
 * ===
 * { "name": "toto", "date": 1234 }
 * { "name": "toto", "date": 1235 }
 */
class SQLiteEval extends SQLiteBase {
  constructor() {
    super();
    this.readonly = false;
  }

  _run(sql, item) {
    try {
      this._checkDB(this.readonly);
      let stmt = this._db.prepare(sql);

      debug('running statement');
      if (stmt.reader) {
        for (var row of stmt.iterate(item)) {
          if (_.isObject(row)) {
            this.emitItem(row);
          }
        }
      }
      else {
        stmt.run(item);
      }
    }
    catch (e) {
      var message = _.get(e, 'message', e);
      debug('SQLite error:', message, 'code:', e.code);
      this.error(message);
    }
  }

  onItem(item) {
    super.onItem(item);

    if (_.has(item, 'sql')) {
      let sql = Item.take(item, 'sql');
      let boundParams = [];

      if (_.isString(sql)) {
        boundParams = this.boundParams(sql);
        this._run(sql, item);
      }
      else if (_.isArray(sql)) {
        _.forEach(sql, (stmt) => {
          boundParams = _.concat(boundParams, this.boundParams(stmt));
          this._run(stmt, item);
        });
      }
      else {
        this.error('invalid sql value type');
      }
      _.forEach(boundParams, function(param) {
        _.unset(item, param);
      });
    }
    if (!_.isEmpty(item)) {
      this.emitItem(item);
    }
  }
}

SQLiteEval.Base = SQLiteBase;

module.exports = SQLiteEval;
