'use strict';
/*
** Copyright (C) 2016 Sylvain Fargier
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2017-02-04T14:02:48+01:00
**     Author: Fargier Sylvain <fargie_s> <fargier.sylvain@free.fr>
*/

const
  path = require('path'),
  _ = require('lodash'),
  fs = require('fs'),
  utils = require('../utils'),
  PipeElement = require('../PipeElement'),
  Item = require('../Item'),
  debug = require('debug')('pipo:files');

/**
 * @module ListDir
 * @description Create a new ListDir
 *
 * ### Configuration
 * | Name       | Type                    | Default | Description                     |
 * | :--------- | :---------------------- | :------ | :------------------------------ |
 * | `pattern`  | RegExp (string or array)| null    | Filename filtering pattern      |
 * | `fullPath` | boolean                 | false   | Emit full-path or only filename |
 *
 * ### Items
 * | Name      | Type   | Description               |
 * | :-------- | :----- | :------------------------ |
 * | `dir`     | string | Set pipe in configuration |
 */
class ListDir extends PipeElement {
  constructor() {
    super();
    this.pattern = null;
    this.fullPath = false;
  }

  onItem(item) {
    super.onItem(item);

    if (_.has(item, 'dir')) {
      var dir = Item.take(item, 'dir');
      debug('listing file in dir:', dir);
      this.ref();
      fs.readdir(dir, (err, files) => {
        if (err) {
          this.error(err);
        } else {
          _.forEach(files, (file) => {
            if (_.isRegExp(this.pattern) && !file.match(this.pattern)) {
              return;
            }
            if (this.fullPath) {
              file = dir + path.sep + file;
            }
            this.emitItem({ file: file });
          });
        }
        this.unref();
      });
    }

    this.emitItem(item);
  }

  setPattern(pattern) {
    utils.setPipePattern(this, 'pattern', pattern);
  }
}

module.exports = ListDir;
